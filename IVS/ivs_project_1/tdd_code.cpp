//======== Copyright (c) 2023, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Test Driven Development - graph
//
// $NoKeywords: $ivs_project_1 $tdd_code.cpp
// $Author:     MARTIN GAENS <xgaens00@stud.fit.vutbr.cz>
// $Date:       $2023-03-07
//============================================================================//
/**
 * @file tdd_code.cpp
 * @author Martin Dočekal
 * @author Karel Ondřej
 *
 * @brief Implementace metod tridy reprezentujici graf.
 */

#include "tdd_code.h"
#include <algorithm>


Graph::Graph() {}

Graph::~Graph() {}

std::vector<Node *> Graph::nodes() {
    return _nodes;
}

std::vector<Edge> Graph::edges() const {
    return _edges;
}

Node *Graph::addNode(size_t nodeId) {
    if (std::any_of(
            _nodes.begin(),
            _nodes.end(),
            [&nodeId](Node *node) { return node->id == nodeId; })) {
        return nullptr;
    }
    Node *node = new Node;
    node->id = nodeId;
    node->color = 0;
    _nodes.push_back(node);
    return node;
}

bool Graph::addEdge(const Edge &edge) {
    if (edge.a == edge.b) return false;
    for (const auto &_edge: _edges) {
        if (edge == _edge) return false;
    }
    if (!getNode(edge.a)) addNode(edge.a);
    if (!getNode(edge.b)) addNode(edge.b);
    _edges.push_back(edge);
    return true;
}

void Graph::addMultipleEdges(const std::vector<Edge> &edges) {
    for (const auto &edge: edges) {
        addEdge(edge);
    }
}

Node *Graph::getNode(size_t nodeId) {
    for (const auto &node: _nodes) {
        if (node->id == nodeId) return node;
    }
    return nullptr;
}

bool Graph::containsEdge(const Edge &edge) const {
    return std::any_of(
            _edges.begin(),
            _edges.end(),
            [&edge](Edge _edge) { return _edge == edge; }
    );
}

void Graph::removeNode(size_t nodeId) {
    int position = -1;
    for (int i = 0; i < _nodes.size(); i++) {
        if (_nodes[i]->id == nodeId) {
            position = i;
            break;
        }
    }
    if (position == -1) {
        throw std::out_of_range("The node with the id " + std::to_string(nodeId) + " wasn't found.");
    }
    _nodes.erase(_nodes.begin() + position);
    _edges.erase(
            std::remove_if(
                    _edges.begin(),
                    _edges.end(),
                    [&nodeId](Edge edge) { return edge.a == nodeId || edge.b == nodeId; }),
            _edges.end()
    );
}

void Graph::removeEdge(const Edge &edge) {
    for (int i = 0; i < _edges.size(); i++) {
        if (_edges[i] == edge) {
            _edges.erase(_edges.begin() + i);
            return;
        }
    }
    throw std::out_of_range("Couldn't find edge.");
}

size_t Graph::nodeCount() const {
    return _nodes.size();
}

size_t Graph::edgeCount() const {
    return _edges.size();
}

size_t Graph::nodeDegree(size_t nodeId) const {
    bool found = false;
    for (const auto &node: _nodes) {
        if (node->id == nodeId) {
            found = true;
            break;
        }
    }
    if (!found) throw std::out_of_range("Could not find node.");
    return std::count_if(
            _edges.begin(),
            _edges.end(),
            [&nodeId](Edge edge) { return edge.a == nodeId || edge.b == nodeId; }
    );
}

size_t Graph::graphDegree() const {
    if (_nodes.empty()) return 0;
    size_t maxDegree = nodeDegree(_nodes[0]->id);
    for (int i = 1; i < _nodes.size(); i++) {
        const size_t degree = nodeDegree(_nodes[i]->id);
        if (degree > maxDegree) maxDegree = degree;
    }
    return maxDegree;
}

void Graph::coloring() {
    /* ospravedlnujem sa */
    auto maxColors = graphDegree() + 1;
    std::cout << maxColors << std::endl;

    for (auto &node : _nodes) node->color = 1;

    outer:
    for (auto edge: _edges) {
        auto node1 = getNode(edge.a);
        auto node2 = getNode(edge.b);
        if (node1->color == node2->color) {
            std::cout << "Colors match! " << node1->color << " " << node2->color << std::endl;
            node1->color = (node1->color + 1) % maxColors + (rand() % maxColors);
            goto outer;
        }
    }
}

void Graph::clear() {
    _nodes.clear();
    _edges.clear();
}

/*** Konec souboru tdd_code.cpp ***/
