//======== Copyright (c) 2022, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     White Box - test suite
//
// $NoKeywords: $ivs_project_1 $white_box_tests.cpp
// $Author:     MARTIN GAENS <xgaens00@stud.fit.vutbr.cz>
// $Date:       $2023-03-07
//============================================================================//
/**
 * @file white_box_tests.cpp
 * @author JMENO PRIJMENI
 * 
 * @brief Implementace testu hasovaci tabulky.
 */

#include <vector>
#include <charconv>

#include "gtest/gtest.h"

#include "white_box_code.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy hasovaci tabulky, testujte nasledujici:
// 1. Verejne rozhrani hasovaci tabulky
//     - Vsechny funkce z white_box_code.h
//     - Chovani techto metod testuje pro prazdnou i neprazdnou tabulku.
// 2. Chovani tabulky v hranicnich pripadech
//     - Otestujte chovani pri kolizich ruznych klicu se stejnym hashem 
//     - Otestujte chovani pri kolizich hashu namapovane na stejne misto v 
//       indexu auto hashMaps = std::vector<hash_map_t *>();
//============================================================================//

class EmptyHashMap : public ::testing::Test {

    void SetUp() override {
        hashMap = hash_map_ctor();
    }

    void TearDown() override {
        hash_map_dtor(hashMap);
    }

protected:
    hash_map_t *hashMap;
};


// kluce 'ckfyujbxbaspreoe' a 'audqpfktjrcsqgfg' maju rovnaky hash
// nas hashtable to zvlada v poriadku
TEST_F(EmptyHashMap, same_hash_different_key) {
    hash_map_put(hashMap, "ckfyujbxbaspreoe", 1);
    hash_map_put(hashMap, "audqpfktjrcsqgfg", 2);
    int output = 0;
    hash_map_get(hashMap, "ckfyujbxbaspreoe", &output);
    EXPECT_EQ(output, 1);
    hash_map_get(hashMap, "audqpfktjrcsqgfg", &output);
    EXPECT_EQ(output, 2);
}

TEST_F(EmptyHashMap, hash_map_ctor) {
    auto hashMaps = std::vector<hash_map_t *>();
    for (int i = 0; i < 1000; i++) {
        hashMaps.push_back(hash_map_ctor());
    }
    for (auto &map: hashMaps) {
        EXPECT_NE(map, nullptr);
        EXPECT_EQ(map->first, nullptr);
        EXPECT_EQ(map->last, nullptr);
        EXPECT_EQ(*map->index, map->first);
        hash_map_dtor(map);
    }
}

TEST_F(EmptyHashMap, hash_map_clear) {
    for (int i = 0; i < 1000; i++) {
        hash_map_put(hashMap, ("key:" + std::to_string(i)).c_str(), 5);
    }
    hash_map_clear(hashMap);
    EXPECT_EQ(hashMap->first, nullptr);
    EXPECT_EQ(hashMap->last, nullptr);
    EXPECT_EQ(hashMap->used, 0);
}

TEST_F(EmptyHashMap, hash_map_dtor) {
    // https://cplusplus.com/forum/general/271859/
    // nenasiel som dobry sposob ako otestovat ci destructor fungoval, pretoze ten struct uz
    // neexistuje a nemam ako zistit, ci free-ing pamati bolo spravne. Mozem pouzit EXPECT_DEATH
    // a dufat, ze nastane neopravneny pristup do pamati ktory sposobi SIGSEGV, ale prislo mi to
    // ako nespolahlivy test.
}

TEST_F(EmptyHashMap, hash_map_reserve_1) {
    EXPECT_EQ(hashMap->allocated, HASH_MAP_INIT_SIZE);
    hash_map_reserve(hashMap, 32);
    EXPECT_EQ(hashMap->allocated, 32);
}

TEST_F(EmptyHashMap, hash_map_reserve_2) {
    for (int i = 0; i < 16; i++) {
        hash_map_put(hashMap, ("key:" + std::to_string(i)).c_str(), i);
    }
    auto result = hash_map_reserve(hashMap, 8);
    EXPECT_EQ(result, VALUE_ERROR);
}

TEST_F(EmptyHashMap, hash_map_size) {
    EXPECT_EQ(hash_map_size(hashMap), 0);
    EXPECT_EQ(hashMap->used, 0);
    for (int i = 0; i < 10; i++) {
        hash_map_put(hashMap, ("key:" + std::to_string(i)).c_str(), i);
    }
    EXPECT_EQ(hashMap->used, 10);
    EXPECT_EQ(hash_map_size(hashMap), 10);
}

TEST_F(EmptyHashMap, hash_map_capacity) {
    EXPECT_EQ(hash_map_capacity(hashMap), HASH_MAP_INIT_SIZE);

    size_t threshold = static_cast<int>((float) HASH_MAP_INIT_SIZE * (float) HASH_MAP_REALLOCATION_THRESHOLD);
    std::cout << threshold << std::endl;
    for (int i = 0; i < threshold + 2; i++) {
        hash_map_put(hashMap, ("key:" + std::to_string(i)).c_str(), i);
    }
    EXPECT_EQ(hash_map_capacity(hashMap), HASH_MAP_INIT_SIZE << 1);
}

TEST_F(EmptyHashMap, hash_map_contains_1) {
    EXPECT_FALSE(hash_map_contains(hashMap, "key:1"));
    hash_map_put(hashMap, "key:1", 1);
    EXPECT_TRUE(hash_map_contains(hashMap, "key:1"));
}

// tieto dva kluce produkuju rovnaky hash
TEST_F(EmptyHashMap, hash_map_contains_2) {
    hash_map_put(hashMap, "ckfyujbxbaspreoe", 1);
    EXPECT_FALSE(hash_map_contains(hashMap, "audqpfktjrcsqgfg"));
}

TEST_F(EmptyHashMap, hash_map_put_1) {
    for (int i = 0; i < 1000; i++) {
        auto result = hash_map_put(hashMap, ("key:" + std::to_string(i)).c_str(), i);
        ASSERT_NE(result, MEMORY_ERROR);
    }
    EXPECT_EQ(hash_map_size(hashMap), 1000);
}

TEST_F(EmptyHashMap, hash_map_put_2) {
    auto result = hash_map_put(hashMap, "key:42", 42);
    ASSERT_NE(result, MEMORY_ERROR);

    result = hash_map_put(hashMap, "key:42", 42);
    ASSERT_NE(result, MEMORY_ERROR);
    EXPECT_EQ(result, KEY_ALREADY_EXISTS);
}

TEST_F(EmptyHashMap, hash_map_get) {
    int output;
    auto result = hash_map_get(hashMap, "key:42", &output);
    EXPECT_EQ(result, KEY_ERROR);
    hash_map_put(hashMap, "key:42", 42);
    hash_map_get(hashMap, "key:42", &output);
    EXPECT_EQ(output, 42);
}

TEST_F(EmptyHashMap, hash_map_remove) {
    auto result = hash_map_remove(hashMap, "key:42");
    EXPECT_EQ(result, KEY_ERROR);
    hash_map_put(hashMap, "key:42", 42);

    auto allocated = hashMap->allocated;
    result = hash_map_remove(hashMap, "key:42");

    EXPECT_EQ(hashMap->allocated, allocated);
    EXPECT_EQ(result, OK);
}

TEST_F(EmptyHashMap, hash_map_pop) {
    auto result = hash_map_remove(hashMap, "key:42");
    EXPECT_EQ(result, KEY_ERROR);
    hash_map_put(hashMap, "key:42", 42);

    auto allocated = hashMap->allocated;
    int output;
    result = hash_map_pop(hashMap, "key:42", &output);

    EXPECT_EQ(hashMap->allocated, allocated);
    EXPECT_EQ(result, OK);
    EXPECT_EQ(output, 42);
}

/*** Konec souboru white_box_tests.cpp ***/
