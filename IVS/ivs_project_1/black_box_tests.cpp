//======== Copyright (c) 2017, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Red-Black Tree - public interface tests
//
// $NoKeywords: $ivs_project_1 $black_box_tests.cpp
// $Author:     MARTIN GAENS <xgaens00@stud.fit.vutbr.cz>
// $Date:       $2017-01-04
//============================================================================//
/**
 * @file black_box_tests.cpp
 * @author MARTIN GAENS
 * 
 * @brief Implementace testu binarniho stromu.
 */

#include <vector>

#include "gtest/gtest.h"

#include "red_black_tree.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy Red-Black Tree, testujte nasledujici:
// 1. Verejne rozhrani stromu
//    - InsertNode/DeleteNode a FindNode
//    - Chovani techto metod testuje pro prazdny i neprazdny strom.
// 2. Axiomy (tedy vzdy platne vlastnosti) Red-Black Tree:
//    - Vsechny listove uzly stromu jsou *VZDY* cerne.
//    - Kazdy cerveny uzel muze mit *POUZE* cerne potomky.
//    - Vsechny cesty od kazdeho listoveho uzlu ke koreni stromu obsahuji
//      *STEJNY* pocet cernych uzlu.
//============================================================================//

class EmptyTree : public ::testing::Test {
protected:
    BinaryTree tree;
};

class NonEmptyTree : public ::testing::Test {

    void SetUp() override {
        tree.InsertNode(INT32_MIN);
        tree.InsertNode(INT32_MAX);
        for (int i = -1000000; i < 1000000; i += 357) {
            if (i > -10 && i < 10) continue;
            tree.InsertNode(i);
        }
    }

protected:
    BinaryTree tree;
};

class TreeAxioms : public ::testing::Test {

    void SetUp() override {
        tree.InsertNode(150);
        tree.InsertNode(200);
        tree.InsertNode(-1000);
        tree.InsertNode(-1000000);
        tree.InsertNode(0);
        tree.InsertNode(INT32_MAX);
        tree.InsertNode(INT32_MIN);
        tree.InsertNode(666);
        tree.InsertNode(667);
        tree.InsertNode(668);
        tree.InsertNode(670);
    }

protected:
    BinaryTree tree;
};

/*** EMPTY TREE ***/

TEST_F(EmptyTree, InsertNode) {
    auto [isNew1, node1] = tree.InsertNode(5);
    EXPECT_TRUE(isNew1);

    auto [isNew2, node2] = tree.InsertNode(3);
    EXPECT_TRUE(isNew2);

    auto [isNew3, node3] = tree.InsertNode(7);
    EXPECT_TRUE(isNew3);

    auto [isNew4, node4] = tree.InsertNode(3);
    EXPECT_FALSE(isNew4);


    EXPECT_EQ(node1->key, 5);
    EXPECT_EQ(node2->key, 3);
    EXPECT_EQ(node3->key, 7);
    EXPECT_EQ(node4, node2);
}

TEST_F(EmptyTree, InsertNodes) {
    std::vector<int> keys = {5, 3, 7, 3};
    std::vector<std::pair<bool, Node_t *>> newNodes;
    tree.InsertNodes(keys, newNodes);

    EXPECT_TRUE(newNodes[0].first);
    EXPECT_TRUE(newNodes[1].first);
    EXPECT_TRUE(newNodes[2].first);
    EXPECT_FALSE(newNodes[3].first);

    EXPECT_EQ(newNodes[0].second->key, 5);
    EXPECT_EQ(newNodes[1].second->key, 3);
    EXPECT_EQ(newNodes[2].second->key, 7);
    EXPECT_EQ(newNodes[3].second, newNodes[1].second);
}

TEST_F(EmptyTree, DeleteNode) {
    auto node= tree.InsertNode(5).second;
    tree.InsertNode(3);
    tree.InsertNode(7);

    EXPECT_TRUE(tree.DeleteNode(3));
    EXPECT_EQ(tree.FindNode(3), nullptr);

    EXPECT_TRUE(tree.DeleteNode(7));
    EXPECT_EQ(tree.FindNode(7), nullptr);

    EXPECT_FALSE(tree.DeleteNode(7));
    EXPECT_FALSE(tree.DeleteNode(0));

    auto nonLeafNodes = std::vector<Node_t *>();
    tree.GetNonLeafNodes(nonLeafNodes);
    EXPECT_EQ(nonLeafNodes.size(), 1);
    EXPECT_EQ(tree.GetRoot(), node);
}

TEST_F(EmptyTree, FindNode) {
    auto node1 = tree.InsertNode(5).second;
    EXPECT_EQ(tree.FindNode(5), node1);
    EXPECT_EQ(tree.FindNode(6), nullptr);

    for (int i = -1000000; i <= 1000000; i += 456) {
        if (i == 3) continue;
        tree.InsertNode(i);
    }
    auto node2 = tree.InsertNode(3).second;
    EXPECT_EQ(tree.FindNode(3), node2);
}


//TEST_F(EmptyTree, GetLeafNodes) {
//    auto node1 = tree.InsertNode(5).second;
//    tree.InsertNode(6);
//    auto node3 = tree.InsertNode(7).second;
//
//    auto leafNodes = std::vector<Node_t *>();
//    tree.GetLeafNodes(leafNodes);
//
//    EXPECT_EQ(leafNodes.size(), 4);
//    EXPECT_EQ(leafNodes[0], node1->pLeft);
//    EXPECT_EQ(leafNodes[1], node1->pRight);
//    EXPECT_EQ(leafNodes[2], node3->pLeft);
//    EXPECT_EQ(leafNodes[3], node3->pRight);
//}
//
//TEST_F(EmptyTree, GetAllNodes) {
//    tree.InsertNode(3);
//    tree.InsertNode(4);
//    tree.InsertNode(5);
//
//    auto allNodes = std::vector<Node_t *>();
//    auto leafNodes = std::vector<Node_t *>();
//    auto nonLeafNodes = std::vector<Node_t *>();
//    tree.GetAllNodes(allNodes);
//    tree.GetLeafNodes(leafNodes);
//    tree.GetNonLeafNodes(nonLeafNodes);
//    EXPECT_EQ(allNodes.size(), leafNodes.size() + nonLeafNodes.size());
//
//    tree.InsertNode(10);
//    allNodes.clear();
//    tree.GetAllNodes(allNodes);
//    EXPECT_EQ(allNodes.size(), 9);
//}
//
//TEST_F(EmptyTree, GetNonLeafNodes) {
//    tree.InsertNode(3);
//    tree.InsertNode(4);
//    tree.InsertNode(5);
//
//    auto nonLeafNodes = std::vector<Node_t *>();
//    tree.GetNonLeafNodes(nonLeafNodes);
//    EXPECT_EQ(nonLeafNodes.size(), 3);
//    EXPECT_EQ(nonLeafNodes[0], tree.GetRoot());
//    EXPECT_EQ(nonLeafNodes[1], tree.GetRoot()->pLeft);
//    EXPECT_EQ(nonLeafNodes[2], tree.GetRoot()->pRight);
//
//    for (int i = 10; i < 1010; i++) tree.InsertNode(i);
//    nonLeafNodes.clear();
//    tree.GetNonLeafNodes(nonLeafNodes);
//    EXPECT_EQ(nonLeafNodes.size(), 1003);
//}

/*** NonEmptyTree tests ***/

TEST_F(NonEmptyTree, InsertNode) {
    auto [isNew1, node1] = tree.InsertNode(5);
    EXPECT_TRUE(isNew1);

    auto [isNew2, node2] = tree.InsertNode(3);
    EXPECT_TRUE(isNew2);

    auto [isNew3, node3] = tree.InsertNode(7);
    EXPECT_TRUE(isNew3);

    auto [isNew4, node4] = tree.InsertNode(3);
    EXPECT_FALSE(isNew4);


    EXPECT_EQ(node1->key, 5);
    EXPECT_EQ(node2->key, 3);
    EXPECT_EQ(node3->key, 7);
    EXPECT_EQ(node4, node2);
}

TEST_F(NonEmptyTree, InsertNodes) {
    std::vector<int> keys = {5, 3, 7, 3};
    std::vector<std::pair<bool, Node_t *>> newNodes;
    tree.InsertNodes(keys, newNodes);

    EXPECT_TRUE(newNodes[0].first);
    EXPECT_TRUE(newNodes[1].first);
    EXPECT_TRUE(newNodes[2].first);
    EXPECT_FALSE(newNodes[3].first);

    EXPECT_EQ(newNodes[0].second->key, 5);
    EXPECT_EQ(newNodes[1].second->key, 3);
    EXPECT_EQ(newNodes[2].second->key, 7);
    EXPECT_EQ(newNodes[3].second, newNodes[1].second);
}

TEST_F(NonEmptyTree, DeleteNode) {
    tree.InsertNode(5);
    tree.InsertNode(3);
    tree.InsertNode(7);

    EXPECT_TRUE(tree.DeleteNode(7));
    EXPECT_EQ(tree.FindNode(7), nullptr);

    EXPECT_FALSE(tree.DeleteNode(7));
    EXPECT_FALSE(tree.DeleteNode(0));

    auto nonLeafNodes = std::vector<Node_t *>();
    tree.GetNonLeafNodes(nonLeafNodes);
    int count = 0;
    for (auto node : nonLeafNodes) count += node->key == 3 ? 1 : 0;
    EXPECT_EQ(count, 1);
}

TEST_F(NonEmptyTree, FindNode) {
    auto node1 = tree.InsertNode(5).second;
    EXPECT_EQ(tree.FindNode(5), node1);
    EXPECT_EQ(tree.FindNode(6), nullptr);

    for (int i = -1000000; i <= 1000000; i += 456) {
        if (i == 3) continue;
        tree.InsertNode(i);
    }
    auto node2 = tree.InsertNode(3).second;
    EXPECT_EQ(tree.FindNode(3), node2);
}


//TEST_F(NonEmptyTree, GetLeafNodes) {
//    auto node1 = tree.InsertNode(5).second;
//    tree.InsertNode(6);
//    auto node3 = tree.InsertNode(7).second;
//
//    auto leafNodes = std::vector<Node_t *>();
//    tree.GetLeafNodes(leafNodes);
//
//    EXPECT_EQ();
//    EXPECT_EQ(leafNodes[1], node1->pRight);
//    EXPECT_EQ(leafNodes[2], node3->pLeft);
//    EXPECT_EQ(leafNodes[3], node3->pRight);
//}
//
//TEST_F(NonEmptyTree, GetAllNodes) {
//    tree.InsertNode(3);
//    tree.InsertNode(4);
//    tree.InsertNode(5);
//
//    auto allNodes = std::vector<Node_t *>();
//    auto leafNodes = std::vector<Node_t *>();
//    auto nonLeafNodes = std::vector<Node_t *>();
//    tree.GetAllNodes(allNodes);
//    tree.GetLeafNodes(leafNodes);
//    tree.GetNonLeafNodes(nonLeafNodes);
//    EXPECT_EQ(allNodes.size(), leafNodes.size() + nonLeafNodes.size());
//
//    tree.InsertNode(10);
//    allNodes.clear();
//    tree.GetAllNodes(allNodes);
//    EXPECT_EQ(allNodes.size(), 9);
//}
//
//TEST_F(NonEmptyTree, GetNonLeafNodes) {
//    tree.InsertNode(3);
//    tree.InsertNode(4);
//    tree.InsertNode(5);
//
//    auto nonLeafNodes = std::vector<Node_t *>();
//    tree.GetNonLeafNodes(nonLeafNodes);
//    EXPECT_EQ(nonLeafNodes.size(), 3);
//    EXPECT_EQ(nonLeafNodes[0], tree.GetRoot());
//    EXPECT_EQ(nonLeafNodes[1], tree.GetRoot()->pLeft);
//    EXPECT_EQ(nonLeafNodes[2], tree.GetRoot()->pRight);
//
//    for (int i = 10; i < 1010; i++) tree.InsertNode(i);
//    nonLeafNodes.clear();
//    tree.GetNonLeafNodes(nonLeafNodes);
//    EXPECT_EQ(nonLeafNodes.size(), 1003);
//}

/*** Testing the tree axioms ***/

TEST_F(TreeAxioms, Axiom1) {
    auto leafNodes = std::vector<Node_t *>();
    tree.GetLeafNodes(leafNodes);

    for (auto leafNode: leafNodes) {
        EXPECT_EQ(leafNode->color, BLACK);
    }
}

TEST_F(TreeAxioms, Axiom2) {
    auto nodes = std::vector<Node_t *>();
    tree.GetAllNodes(nodes);

    for (auto node: nodes) {
        if (node->color == RED) {
            EXPECT_EQ(node->pRight->color, BLACK);
            EXPECT_EQ(node->pLeft->color, BLACK);
        }
    }
}

TEST_F(TreeAxioms, Axiom3) {
    auto leafNodes = std::vector<Node_t *>();
    tree.GetLeafNodes(leafNodes);

    int firstBlacks = 0;
    bool first = true;
    for (auto node: leafNodes) {
        int blacks = 0;
        auto customNode = node;
        while (customNode->pParent != nullptr) {
            if (customNode->color == BLACK) blacks++;
            customNode = customNode->pParent;
        }
        if (first) {
            firstBlacks = blacks;
            first = false;
        }
        EXPECT_EQ(blacks, firstBlacks);
    }
}

/*** Konec souboru black_box_tests.cpp ***/
