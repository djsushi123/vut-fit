LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;


ENTITY UART_RX_FSM IS
    port(
        CLK      : IN std_logic;
        RST      : IN std_logic;
        DIN      : IN std_logic;
        BIT_END  : IN std_logic;
        WORD_END : IN std_logic;
        DATA     : OUT std_logic;
        CLK_ADD  : OUT std_logic;
        VALID    : OUT std_logic -- when we're receiving data and DIN goes to 1
    );
END ENTITY;

ARCHITECTURE behavioral OF UART_RX_FSM IS
    TYPE state_t IS (STATE_IDLE, STATE_START_BIT, STATE_DATA, STATE_STOP_BIT);
    SIGNAL current_state, next_state : state_t;
BEGIN
    updateState: PROCESS(CLK, RST) BEGIN
        IF (RST = '1') THEN
            current_state <= STATE_IDLE;
        ELSIF rising_edge(CLK) THEN
            current_state <= next_state;
        END IF;
    END PROCESS;

    outputs: PROCESS(current_state) BEGIN
        CASE current_state IS
            WHEN STATE_IDLE =>
                CLK_ADD <= '0';
                DATA    <= '0';
                VALID   <= '0';
            WHEN STATE_START_BIT =>
                CLK_ADD <= '1';
                DATA    <= '0';
                VALID   <= '0';
            WHEN STATE_DATA =>
                CLK_ADD <= '1';
                DATA    <= '1';
                VALID   <= '0';
            WHEN STATE_STOP_BIT =>
                CLK_ADD <= '1';
                DATA    <= '0';
                VALID   <= '1';
        END CASE;
    END PROCESS;

    findNextState: PROCESS(DIN, current_state, BIT_END, WORD_END) BEGIN
        next_state <= current_state; -- Default state IS current state
        
        IF current_state = STATE_IDLE AND DIN = '0' THEN
            next_state <= STATE_START_BIT;
        END IF;
        
        IF current_state = STATE_START_BIT AND BIT_END = '1' THEN
            next_state <= STATE_DATA;
        END IF;
        
        IF current_state = STATE_DATA AND BIT_END = '1' AND WORD_END = '1' THEN
            next_state <= STATE_STOP_BIT;
        END IF;
        
        IF current_state = STATE_STOP_BIT AND BIT_END = '1' AND DIN = '1' THEN
            next_state <= STATE_IDLE;
        END IF;
    END PROCESS findNextState;
END ARCHITECTURE;
