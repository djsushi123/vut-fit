package ija.ija2023.homework1.room;

import ija.ija2023.homework1.common.Environment;
import ija.ija2023.homework1.common.Position;
import ija.ija2023.homework1.common.Robot;

import java.util.ArrayList;

public class Room implements Environment {

    private final int rows, cols;
    private final ArrayList<Position> obstacles = new ArrayList<>();
    private final ArrayList<Position> robots = new ArrayList<>();

    private Room(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
    }

    public static Room create(int rows, int cols) {
        if (rows < 1 || cols < 1) {
            throw new IllegalArgumentException("The number of rows or cols is incorrect.");
        }
        return new Room(rows, cols);
    }

    @Override
    public boolean addRobot(Robot robot) {
        Position robotPos = robot.getPosition();
        if (!containsPosition(robotPos)) return false;
        robots.add(robotPos);
        return true;
    }

    @Override
    public boolean containsPosition(Position pos) {
        return pos.getRow() >= 0 && pos.getRow() < rows && pos.getCol() >= 0 && pos.getCol() < cols;
    }

    @Override
    public boolean createObstacleAt(int row, int col) {
        Position position = new Position(row, col);
        if (obstacles.contains(position)) {
            return false; // Obstacle already exists at this position
        }
        obstacles.add(position);
        return true;
    }

    @Override
    public boolean obstacleAt(int row, int col) {
        return obstacles.contains(new Position(row, col));
    }

    @Override
    public boolean obstacleAt(Position p) {
        return obstacles.contains(p);
    }

    @Override
    public boolean robotAt(Position p) {
        return robots.contains(p);
    }
}
