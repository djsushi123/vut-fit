package ija.ija2023.homework1.room;

import ija.ija2023.homework1.common.Environment;
import ija.ija2023.homework1.common.Position;
import ija.ija2023.homework1.common.Robot;

public class ControlledRobot implements Robot {

    private Position pos;
    private int angle = 0;
    private Environment env;

    private ControlledRobot(Position pos, Environment env) {
        this.pos = pos;
        this.env = env;
    }

    public static ControlledRobot create(Environment env, Position pos) {
        if (env.obstacleAt(pos.getRow(), pos.getCol()) || env.robotAt(pos)) {
            return null; // Robot cannot be created at an obstacle position
        }

        ControlledRobot robot = new ControlledRobot(pos, env);
        env.addRobot(robot);
        return robot;
    }

    @Override
    public int angle() {
        return angle;
    }

    private Position nextPosition() {
        int rowDiff = switch (angle) {
            case 90, 270 -> 0;
            case 135, 180, 225 -> 1;
            case 315, 0, 45 -> -1;
            default -> throw new IllegalStateException("Unexpected value: " + angle);
        };

        int colDiff = switch (angle) {
            case 180, 0 -> 0;
            case 90, 45, 135 -> 1;
            case 225, 270, 315 -> -1; // TODO cool ked nastavis 215 tak IDEcko povie unreachable code
            default -> throw new IllegalStateException("Unexpected value: " + angle);
        };

        return new Position(pos.getRow() + rowDiff, pos.getCol() + colDiff);
    }

    @Override
    public boolean canMove() {
        Position nextPos = nextPosition();

        if (!env.containsPosition(nextPos)) return false;
        if (env.robotAt(nextPos)) return false;
        if (env.obstacleAt(nextPos)) return false;

        return true;
    }

    @Override
    public Position getPosition() {
        return pos;
    }

    @Override
    public boolean move() {
        if (!canMove()) return false;
        pos = nextPosition();
        return true;
    }

    @Override
    public void turn() {
        angle = (angle + 45) % 360;
    }
}
