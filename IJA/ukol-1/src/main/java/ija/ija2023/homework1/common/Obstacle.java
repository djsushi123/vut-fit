package ija.ija2023.homework1.common;

public class Obstacle {
    private final Position pos;

    public Obstacle(Environment env, Position pos) {
        this.pos = pos;
    }

    public Position getPos() {
        return pos;
    }
}
