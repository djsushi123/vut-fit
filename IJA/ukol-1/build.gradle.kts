plugins {
    id("java")
}

group = "ija.ija2023.homework1"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.platform:junit-platform-console-standalone:1.6.0")
}

tasks.test {
    useJUnitPlatform()
}