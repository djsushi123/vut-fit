import asyncio
import aiohttp


async def get_urls(urls: list[str]):
    async def get_url(url):
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(url) as response:
                    return response.status, url
        except aiohttp.ClientError as e:
            return f'{type(e).__module__}.{type(e).__name__}', url

    tasks = []
    async with aiohttp.ClientSession():
        for url in urls:
            task = asyncio.ensure_future(get_url(url))
            tasks.append(task)
        results = await asyncio.gather(*tasks)
        return results


if __name__ == '__main__':
    urls = ['https://www.fit.vutbr.cz', 'https://www.szn.cz', 'https://www.alza.cz', 'https://office.com', 'https://aukro.cz']
    res = asyncio.run(get_urls(urls))
    print(res)
