#!/usr/bin/env python3
from typing import Sequence


def gen_quiz(qpool: list[tuple[str, list[str]]], *args, **kwargs):
    altcodes: Sequence[str] = kwargs.get('altcodes', [])
    existing_quiz = kwargs.get('quiz', [])
    out = []
    out.extend(existing_quiz)
    for index in args:
        try:
            question = qpool[index][0]
            answers = qpool[index][1]
        except Exception as e:
            print("Ignoring index", index, "-", e)
            continue
        out.append((question, [f"{altcode}: {question}" for altcode, question in zip(altcodes, answers)]))
    return out
