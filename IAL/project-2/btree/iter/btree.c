/*
 * Binární vyhledávací strom — iterativní varianta
 *
 * S využitím datových typů ze souboru btree.h, zásobníku ze souboru stack.h 
 * a připravených koster funkcí implementujte binární vyhledávací 
 * strom bez použití rekurze.
 */

#include "../btree.h"
#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * Inicializace stromu.
 *
 * Uživatel musí zajistit, že inicializace se nebude opakovaně volat nad
 * inicializovaným stromem. V opačném případě může dojít k úniku paměti (memory
 * leak). Protože neinicializovaný ukazatel má nedefinovanou hodnotu, není
 * možné toto detekovat ve funkci. 
 */
void bst_init(bst_node_t **tree) {
    *tree = NULL;
}

/*
 * Vyhledání uzlu v stromu.
 *
 * V případě úspěchu vrátí funkce hodnotu true a do proměnné value zapíše
 * hodnotu daného uzlu. V opačném případě funkce vrátí hodnotu false a proměnná
 * value zůstává nezměněná.
 * 
 * Funkci implementujte iterativně bez použité vlastních pomocných funkcí.
 */
bool bst_search(bst_node_t *tree, char key, int *value) {
    bst_node_t *current_node = tree;
    while (current_node != NULL) {
        if (key < current_node->key) current_node = current_node->left;
        else if (key > current_node->key) current_node = current_node->right;
        else {
            *value = current_node->value;
            return true;
        }
    }
    return false;
}

/*
 * Vložení uzlu do stromu.
 *
 * Pokud uzel se zadaným klíče už ve stromu existuje, nahraďte jeho hodnotu.
 * Jinak vložte nový listový uzel.
 *
 * Výsledný strom musí splňovat podmínku vyhledávacího stromu — levý podstrom
 * uzlu obsahuje jenom menší klíče, pravý větší. 
 *
 * Funkci implementujte iterativně bez použití vlastních pomocných funkcí.
 */
void bst_insert(bst_node_t **tree, char key, int value) {
    if (*tree == NULL) {
        bst_node_t *new_node = malloc(sizeof(bst_node_t));
        if (new_node == NULL) return;
        new_node->key = key;
        new_node->value = value;
        new_node->left = NULL;
        new_node->right = NULL;
        *tree = new_node;
        return;
    }
    bst_node_t *current_node = *tree;
    while (current_node->key != key) {
        if (key < current_node->key) {
            if (current_node->left == NULL) {
                bst_node_t *new_node = malloc(sizeof(bst_node_t));
                if (new_node == NULL) return;
                new_node->key = key;
                new_node->value = value;
                new_node->left = NULL;
                new_node->right = NULL;
                current_node->left = new_node;
                return;
            } else {
                current_node = current_node->left;
            }
        } else {
            if (current_node->right == NULL) {
                bst_node_t *new_node = malloc(sizeof(bst_node_t));
                if (new_node == NULL) return;
                new_node->key = key;
                new_node->value = value;
                new_node->left = NULL;
                new_node->right = NULL;
                current_node->right = new_node;
                return;
            } else {
                current_node = current_node->right;
            }
        }
    }
    current_node->value = value;
}

/*
 * Pomocná funkce která nahradí uzel nejpravějším potomkem.
 * 
 * Klíč a hodnota uzlu target budou nahrazené klíčem a hodnotou nejpravějšího
 * uzlu podstromu tree. Nejpravější potomek bude odstraněný. Funkce korektně
 * uvolní všechny alokované zdroje odstraněného uzlu.
 *
 * Funkce předpokládá, že hodnota tree není NULL.
 * 
 * Tato pomocná funkce bude využita při implementaci funkce bst_delete.
 *
 * Funkci implementujte iterativně bez použití vlastních pomocných funkcí.
 */
void bst_replace_by_rightmost(bst_node_t *target, bst_node_t **tree) {
    // we get a subtree, and we want to traverse all the way to the right until node->right is NULL, and then, we
    // want to replace the values of target to the new values
    bst_node_t *current_node = *tree;
    if (current_node->right == NULL) {
        target->key = current_node->key;
        target->value = current_node->value;
        target->left = current_node->left;
        free(current_node);
        return;
    }
    while (current_node->right->right != NULL) {
        current_node = current_node->right;
    }
    bst_node_t *rightmost = current_node->right;
    target->key = rightmost->key;
    target->value = rightmost->value;
    current_node->right = rightmost->left;
    free(rightmost);
}

/*
 * Odstranění uzlu ze stromu.
 *
 * Pokud uzel se zadaným klíčem neexistuje, funkce nic nedělá.
 * Pokud má odstraněný uzel jeden podstrom, zdědí ho rodič odstraněného uzlu.
 * Pokud má odstraněný uzel oba podstromy, je nahrazený nejpravějším uzlem
 * levého podstromu. Nejpravější uzel nemusí být listem.
 * 
 * Funkce korektně uvolní všechny alokované zdroje odstraněného uzlu.
 * 
 * Funkci implementujte iterativně pomocí bst_replace_by_rightmost a bez
 * použití vlastních pomocných funkcí.
 */
void bst_delete(bst_node_t **tree, char key) {
    bst_node_t *current_node = *tree;
    bst_node_t *previous_node = NULL;
    bool went_left;

    // traverse down the tree until we find the matching key, or return if no key found
    while (current_node != NULL && current_node->key != key) {
        previous_node = current_node;
        if (key < current_node->key) {
            current_node = current_node->left;
            went_left = true;
        } else {
            current_node = current_node->right;
            went_left = false;
        }
    }
    if (current_node == NULL) return; // we didn't find a match
    if (previous_node == NULL) { // in case we want to delete the root
        if (current_node->left != NULL && current_node->right != NULL) {
            bst_replace_by_rightmost(current_node, &current_node->left);
            return;
        } else if (current_node->left != NULL) {
            *tree = current_node->left;
        } else if (current_node->right != NULL) {
            *tree = current_node->right;
        } else { // we've got a leaf node deletion situation here
            *tree = NULL;
        }
        free(current_node);
        return;
    }

    // now, we have a current node and a previous node.
    if (current_node->left != NULL && current_node->right != NULL) {
        bst_replace_by_rightmost(current_node, &current_node->left);
        return;
    } else if (current_node->left != NULL) {
        if (went_left) previous_node->left = current_node->left;
        else previous_node->right = current_node->left;
    } else if (current_node->right != NULL) {
        if (went_left) previous_node->left = current_node->right;
        else previous_node->right = current_node->right;
    } else { // we've got a leaf node deletion situation here
        if (went_left) previous_node->left = NULL;
        else previous_node->right = NULL;
    }
    free(current_node);
}

/*
 * Zrušení celého stromu.
 * 
 * Po zrušení se celý strom bude nacházet ve stejném stavu jako po 
 * inicializaci. Funkce korektně uvolní všechny alokované zdroje rušených 
 * uzlů.
 * 
 * Funkci implementujte iterativně s pomocí zásobníku a bez použití 
 * vlastních pomocných funkcí.
 */
void bst_dispose(bst_node_t **tree) {
    stack_bst_t stack;
    stack_bst_init(&stack);
    bst_node_t *current_node;

    while (!stack_bst_empty(&stack) || *tree != NULL) {
        if (*tree == NULL) {
            *tree = stack_bst_top(&stack);
            stack_bst_pop(&stack);
        }
        if ((*tree)->right) stack_bst_push(&stack, (*tree)->right);
        current_node = *tree;
        *tree = (*tree)->left;
        free(current_node);
    }
}

/*
 * Pomocná funkce pro iterativní preorder.
 *
 * Prochází po levé větvi k nejlevějšímu uzlu podstromu.
 * Nad zpracovanými uzly zavolá bst_add_node_to_items a uloží je do zásobníku uzlů.
 *
 * Funkci implementujte iterativně s pomocí zásobníku a bez použití 
 * vlastních pomocných funkcí.
 */
void bst_leftmost_preorder(bst_node_t *tree, stack_bst_t *to_visit, bst_items_t *items) {
    while (tree != NULL) {
        stack_bst_push(to_visit, tree);
        bst_add_node_to_items(tree, items);
        tree = tree->left;
    }
}

/*
 * Preorder průchod stromem.
 *
 * Pro aktuálně zpracovávaný uzel zavolejte funkci bst_add_node_to_items.
 *
 * Funkci implementujte iterativně pomocí funkce bst_leftmost_preorder a
 * zásobníku uzlů a bez použití vlastních pomocných funkcí.
 */
void bst_preorder(bst_node_t *tree, bst_items_t *items) {
    stack_bst_t stack;
    stack_bst_init(&stack);

    bst_leftmost_preorder(tree, &stack, items);
    while (!stack_bst_empty(&stack)) {
        tree = stack_bst_pop(&stack);
        bst_leftmost_preorder(tree->right, &stack, items);
    }
}

/*
 * Pomocná funkce pro iterativní inorder.
 * 
 * Prochází po levé větvi k nejlevějšímu uzlu podstromu a ukládá uzly do
 * zásobníku uzlů.
 *
 * Funkci implementujte iterativně s pomocí zásobníku a bez použití 
 * vlastních pomocných funkcí.
 */
void bst_leftmost_inorder(bst_node_t *tree, stack_bst_t *to_visit) {
    while (tree != NULL) {
        stack_bst_push(to_visit, tree);
        tree = tree->left;
    }
}

/*
 * Inorder průchod stromem.
 *
 * Pro aktuálně zpracovávaný uzel zavolejte funkci bst_add_node_to_items.
 *
 * Funkci implementujte iterativně pomocí funkce bst_leftmost_inorder a
 * zásobníku uzlů a bez použití vlastních pomocných funkcí.
 */
void bst_inorder(bst_node_t *tree, bst_items_t *items) {
    stack_bst_t stack;
    stack_bst_init(&stack);

    bst_leftmost_inorder(tree, &stack);
    while (!stack_bst_empty(&stack)) {
        tree = stack_bst_top(&stack);
        stack_bst_pop(&stack);
        bst_add_node_to_items(tree, items);
        bst_leftmost_inorder(tree->right, &stack);
    }
}

/*
 * Pomocná funkce pro iterativní postorder.
 *
 * Prochází po levé větvi k nejlevějšímu uzlu podstromu a ukládá uzly do
 * zásobníku uzlů. Do zásobníku bool hodnot ukládá informaci, že uzel
 * byl navštíven poprvé.
 *
 * Funkci implementujte iterativně pomocí zásobníku uzlů a bool hodnot a bez použití
 * vlastních pomocných funkcí.
 */
void bst_leftmost_postorder(bst_node_t *tree, stack_bst_t *to_visit, stack_bool_t *first_visit) {
    while (tree != NULL) {
        stack_bool_push(first_visit, true);
        stack_bst_push(to_visit, tree);
        tree = tree->left;
    }
}

/*
 * Postorder průchod stromem.
 *
 * Pro aktuálně zpracovávaný uzel zavolejte funkci bst_add_node_to_items.
 *
 * Funkci implementujte iterativně pomocí funkce bst_leftmost_postorder a
 * zásobníku uzlů a bool hodnot a bez použití vlastních pomocných funkcí.
 */
void bst_postorder(bst_node_t *tree, bst_items_t *items) {
    stack_bst_t stack_bst;
    stack_bool_t stack_bool;
    stack_bst_init(&stack_bst);
    stack_bool_init(&stack_bool);

    bst_leftmost_postorder(tree, &stack_bst, &stack_bool);
    while (!stack_bst_empty(&stack_bst)) {
        bool visited_once = stack_bool_top(&stack_bool);
        stack_bool_pop(&stack_bool);
        tree = stack_bst_top(&stack_bst);
        stack_bst_pop(&stack_bst);
        if (visited_once) {
            stack_bst_push(&stack_bst, tree);
            stack_bool_push(&stack_bool, false);
            bst_leftmost_postorder(tree->right, &stack_bst, &stack_bool);
            continue;
        }

        bst_add_node_to_items(tree, items);
    }
}
