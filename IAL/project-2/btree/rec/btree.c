/*
 * Binární vyhledávací strom — rekurzivní varianta
 *
 * S využitím datových typů ze souboru btree.h a připravených koster funkcí
 * implementujte binární vyhledávací strom pomocí rekurze.
 */

#include "../btree.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * Inicializace stromu.
 *
 * Uživatel musí zajistit, že inicializace se nebude opakovaně volat nad
 * inicializovaným stromem. V opačném případě může dojít k úniku paměti (memory
 * leak). Protože neinicializovaný ukazatel má nedefinovanou hodnotu, není
 * možné toto detekovat ve funkci. 
 */
void bst_init(bst_node_t **tree) {
    *tree = NULL;
}

/*
 * Vyhledání uzlu v stromu.
 *
 * V případě úspěchu vrátí funkce hodnotu true a do proměnné value zapíše
 * hodnotu daného uzlu. V opačném případě funkce vrátí hodnotu false a proměnná
 * value zůstává nezměněná.
 * 
 * Funkci implementujte rekurzivně bez použité vlastních pomocných funkcí.
 */
bool bst_search(bst_node_t *tree, char key, int *value) {
    if (tree == NULL) return false;
    if (tree->key == key) {
        *value = tree->value;
        return true;
    } else if (key < tree->key) {
        return bst_search(tree->left, key, value);
    } else {
        return bst_search(tree->right, key, value);
    }
}

/*
 * Vložení uzlu do stromu.
 *
 * Pokud uzel se zadaným klíče už ve stromu existuje, nahraďte jeho hodnotu.
 * Jinak vložte nový listový uzel.
 *
 * Výsledný strom musí splňovat podmínku vyhledávacího stromu — levý podstrom
 * uzlu obsahuje jenom menší klíče, pravý větší. 
 *
 * Funkci implementujte rekurzivně bez použití vlastních pomocných funkcí.
 */
void bst_insert(bst_node_t **tree, char key, int value) {
    // if tree empty, make this the new root
    if (*tree == NULL) {
        bst_node_t *new_root = malloc(sizeof(bst_node_t));
        if (new_root == NULL) return;
        new_root->key = key;
        new_root->value = value;
        new_root->left = NULL;
        new_root->right = NULL;
        *tree = new_root;
    }
    // in case tree not empty, recurse in the correct direction until the tree is "empty"
    if ((*tree)->key == key) {
        (*tree)->value = value;
    } else if (key < (*tree)->key) {
        bst_insert(&((*tree)->left), key, value);
    } else {
        bst_insert(&((*tree)->right), key, value);
    }
}

/*
 * Pomocná funkce která nahradí uzel nejpravějším potomkem.
 * 
 * Klíč a hodnota uzlu target budou nahrazeny klíčem a hodnotou nejpravějšího
 * uzlu podstromu tree. Nejpravější potomek bude odstraněný. Funkce korektně
 * uvolní všechny alokované zdroje odstraněného uzlu.
 *
 * Funkce předpokládá, že hodnota tree není NULL.
 * 
 * Tato pomocná funkce bude využitá při implementaci funkce bst_delete.
 *
 * Funkci implementujte rekurzivně bez použití vlastních pomocných funkcí.
 */
void bst_replace_by_rightmost(bst_node_t *target, bst_node_t **tree) {
    if ((*tree)->right == NULL) {
        target->key = (*tree)->key;
        target->value = (*tree)->value;
        target->left = (*tree)->left;
        free(*tree);
    } else if ((*tree)->right->right == NULL) { // we reached the bottom
        bst_node_t *rightmost = (*tree)->right;
        target->key = rightmost->key;
        target->value = rightmost->value;
        (*tree)->right = rightmost->left;
        free(rightmost);
    } else {
        bst_replace_by_rightmost(target, &((*tree)->right));
    }
}

/*
 * Odstranění uzlu ze stromu.
 *
 * Pokud uzel se zadaným klíčem neexistuje, funkce nic nedělá.
 * Pokud má odstraněný uzel jeden podstrom, zdědí ho rodič odstraněného uzlu.
 * Pokud má odstraněný uzel oba podstromy, je nahrazený nejpravějším uzlem
 * levého podstromu. Nejpravější uzel nemusí být listem.
 * 
 * Funkce korektně uvolní všechny alokované zdroje odstraněného uzlu.
 * 
 * Funkci implementujte rekurzivně pomocí bst_replace_by_rightmost a bez
 * použití vlastních pomocných funkcí.
 */
void bst_delete(bst_node_t **tree, char key) {
    if (*tree == NULL) return;

    if (key < (*tree)->key) {
        if ((*tree)->left == NULL || (*tree)->left->key != key) {
            bst_delete(&((*tree)->left), key);
            return;
        }

        bst_node_t *to_delete = (*tree)->left;

        if (to_delete->left == NULL && to_delete->right == NULL) (*tree)->left = NULL;
        else if (to_delete->left != NULL && to_delete->right == NULL) (*tree)->left = to_delete->left;
        else if (to_delete->left == NULL && to_delete->right != NULL)(*tree)->left = to_delete->right;
        else {
            // no freeing here because it's done in bst_replace_by_rightmost
            bst_replace_by_rightmost(to_delete, &(to_delete->left));
            return;
        }
        free(to_delete);
    } else if (key > (*tree)->key) {
        if ((*tree)->right == NULL || (*tree)->right->key != key) {
            bst_delete(&((*tree)->right), key);
            return;
        }

        bst_node_t *to_delete = (*tree)->right;

        if (to_delete->left != NULL && to_delete->right == NULL) (*tree)->right = to_delete->left;
        else if (to_delete->left == NULL && to_delete->right != NULL) (*tree)->right = to_delete->right;
        else if (to_delete->left == NULL && to_delete->right == NULL) (*tree)->right = NULL;
        else {
            // no freeing here because it's done in bst_replace_by_rightmost
            bst_replace_by_rightmost(to_delete, &(to_delete->left));
            return;
        }

        free(to_delete);
    } else {
        bst_node_t *to_delete = *tree;

        if (to_delete->left == NULL && to_delete->right == NULL) *tree = NULL;
        else if (to_delete->left && !to_delete->right) *tree = to_delete->left;
        else if (!to_delete->left && to_delete->right) *tree = to_delete->right;
        else {
            // no freeing here because it's done in bst_replace_by_rightmost
            bst_replace_by_rightmost(to_delete, &(to_delete->left));
            return;
        }

        free(to_delete);
    }
}

/*
 * Zrušení celého stromu.
 * 
 * Po zrušení se celý strom bude nacházet ve stejném stavu jako po 
 * inicializaci. Funkce korektně uvolní všechny alokované zdroje rušených 
 * uzlů.
 * 
 * Funkci implementujte rekurzivně bez použití vlastních pomocných funkcí.
 */
void bst_dispose(bst_node_t **tree) {
    if (*tree == NULL) return;
    bst_dispose(&((*tree)->left));
    bst_dispose(&((*tree)->right));
    free(*tree);
    *tree = NULL;
}

/*
 * Preorder průchod stromem.
 *
 * Pro aktuálně zpracovávaný uzel zavolejte funkci bst_add_node_to_items.
 *
 * Funkci implementujte rekurzivně bez použití vlastních pomocných funkcí.
 */
void bst_preorder(bst_node_t *tree, bst_items_t *items) {
    // Root, Left, Right
    bst_add_node_to_items(tree, items);
    if (tree->left != NULL) bst_preorder(tree->left, items);
    if (tree->right != NULL) bst_preorder(tree->right, items);
}

/*
 * Inorder průchod stromem.
 *
 * Pro aktuálně zpracovávaný uzel zavolejte funkci bst_add_node_to_items.
 *
 * Funkci implementujte rekurzivně bez použití vlastních pomocných funkcí.
 */
void bst_inorder(bst_node_t *tree, bst_items_t *items) {
    // Left, Root, Right
    if (tree->left != NULL) bst_inorder(tree->left, items);
    bst_add_node_to_items(tree, items);
    if (tree->right != NULL) bst_inorder(tree->right, items);
}

/*
 * Postorder průchod stromem.
 *
 * Pro aktuálně zpracovávaný uzel zavolejte funkci bst_add_node_to_items.
 *
 * Funkci implementujte rekurzivně bez použití vlastních pomocných funkcí.
 */
void bst_postorder(bst_node_t *tree, bst_items_t *items) {
    // Left, Right, Root
    if (tree->left != NULL) bst_postorder(tree->left, items);
    if (tree->right != NULL) bst_postorder(tree->right, items);
    bst_add_node_to_items(tree, items);
}
