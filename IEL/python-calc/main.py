def convert_number_to_text(number: int) -> str:
    # Slovak words for the digits 0-9
    digits = ['nula', 'jeden', 'dva', 'tri', 'štyri', 'päť', 'šesť', 'sedem', 'osem', 'deväť']
    # Slovak words for the tens (10, 20, 30, etc.)
    tens = ['desať', 'dvadsať', 'tridsať', 'štyridsať', 'päťdesiat', 'šesťdesiat', 'sedemdesiat', 'osemdesiat',
            'deväťdesiat']
    # Slovak words for the hundreds (100, 200, 300, etc.)
    hundreds = ['sto', 'dvesto', 'tristo', 'štyristo', 'päťsto', 'šesťsto', 'sedemsto', 'osemsto', 'deväťsto']
    # Slovak words for the thousands (1000, 2000, 3000, etc.)
    thousands = ['tisíc', 'tisíce', 'tisíce', 'tisíce', 'tisíc', 'tisíc', 'tisíc', 'tisíc', 'tisíc']
    # Slovak words for the millions (1000000, 2000000, 3000000, etc.)
    millions = ['milión', 'milióny', 'milióny', 'milióny', 'miliónov', 'miliónov', 'miliónov', 'miliónov', 'miliónov']

    if number < 0:
        return 'mínus' + convert_number_to_text(-number)
    elif number < 10:
        return digits[number]
    elif number < 100:
        if number % 10 == 0:
            return tens[number // 10 - 1]
        else:
            return tens[number // 10 - 1] + digits[number % 10]
    elif number < 1000:
        if number % 100 == 0:
            return hundreds[number // 100 - 1]
        else:
            return hundreds[number // 100 - 1] + convert_number_to_text(number % 100)
    elif number < 1000000:
        if number % 1000 == 0:
            return convert_number_to_text(number // 1000) + thousands[number // 1000 - 1]
        else:
            return convert_number_to_text(number // 1000) + thousands[number // 1000 - 1] + convert_number_to_text(
                number % 1000)
    elif number < 1000000000:
        if number % 1000000 == 0:
            return convert_number_to_text(number // 1000000) + millions[number // 1000000 - 1]
        else:
            return convert_number_to_text(number // 1000000) + millions[number // 1000000 - 1] + convert_number_to_text(
                number % 1000000)


if __name__ == '__main__':
    print(convert_number_to_text(58556))
