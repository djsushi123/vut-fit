#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include "proj2.h"

#define OUTPUT_FILE_NAME "proj2.out"
//#define DEBUG

#ifndef DEBUG

#define debug(msg, ...)
#define action(msg, ...) \
    sem_wait(sem_action); \
    fprintf(output_file, "%d: " msg "\n", (*current_action_number)++, ##__VA_ARGS__); \
    fflush(output_file); \
    sem_post(sem_action)

#else

#define debug(msg, ...) fprintf(stderr, "DEBUG %s:%d: " msg "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define action(msg, ...) \
    sem_wait(sem_action); \
    printf("%d: " msg "\n", (*current_action_number)++, ##__VA_ARGS__); \
    sem_post(sem_action)

#endif

//****************************************** SEMAPHORE LOGIC ****************************************************/

#define SEM_ACTION "xgaens00_SEM_ACTION"
#define SEM_QUEUE "xgaens00_SEM_QUEUE"
#define SEM_QUEUE_WAITING_NUMBER "xgaens00_SEM_QUEUE_WAITING_NUMBER"
#define SEM_OFFICE_OPEN "xgaens00_SEM_OFFICE_OPEN"

/// Semaphore for the actions, ensures that the actions are printed in the correct order and not interleaved
sem_t *sem_action;

/// An array of semaphores for the queues, one for each queue
sem_t *sems_queue[3];

/// Semaphore to prevent events such as customer entering and the last available official taking a break
sem_t *sem_queue_waiting_number;

/// Semaphore for the office, ensures that multiple processes don't write to the 'office_open' variable at the same time
sem_t *sem_office_open;

/**
 * Starts and returns the semaphore with the given name.
 */
sem_t *sem_start(char *name, int32_t initial_value) {
    sem_unlink(name);
    return sem_open(name, O_CREAT, 0666, initial_value);
}

/**
 * Initializes the semaphores.
 * @returns true on success, false on failure
 */
bool init_semaphores(void) {
    sem_action = sem_start(SEM_ACTION, 1);
    sem_office_open = sem_start(SEM_OFFICE_OPEN, 1);
    sem_queue_waiting_number = sem_start(SEM_QUEUE_WAITING_NUMBER, 1);

    char sem_name[30];
    for (int i = 0; i < 3; ++i) {
        sprintf(sem_name, "%s_%d", SEM_QUEUE, i);
        sems_queue[i] = sem_start(sem_name, 0);
    }

    return sem_action != SEM_FAILED && sem_office_open != SEM_FAILED &&
           sem_queue_waiting_number != SEM_FAILED && sems_queue[0] != SEM_FAILED && sems_queue[1] != SEM_FAILED &&
           sems_queue[2] != SEM_FAILED;
}

/**
 * Closes and unlinks the semaphores.
 */
void remove_semaphores(void) {
    sem_close(sem_action);
    sem_unlink(SEM_ACTION);

    sem_close(sem_office_open);
    sem_unlink(SEM_OFFICE_OPEN);

    sem_close(sem_queue_waiting_number);
    sem_unlink(SEM_QUEUE_WAITING_NUMBER);

    char sem_queue_name[30];
    for (int i = 0; i < 3; ++i) {
        sprintf(sem_queue_name, "%s_%d", SEM_QUEUE, i);
        sem_close(sems_queue[i]);
        sem_unlink(sem_queue_name);
    }
}

//****************************************** SHARED MEMORY LOGIC ****************************************************/

bool *office_open;
bool *everything_ok;
u_int32_t *current_action_number;

/// This holds the number of customers waiting in each queue.
u_int32_t *queue_waiting_number;


// ========== Globals for each process to use ==========
u_int32_t customer_id = 0, official_id = 0; // zero means uninitialized since IDs start from 1
u_int32_t n_customers, n_officials, max_customer_wait_time, max_official_break_time, max_office_open_time;

FILE *output_file;

/**
 * Initializes the variables that are supposed to be inside shared memory
 */
void init_shared(void) {
    office_open = create_shared_memory(sizeof(bool));
    *office_open = true;
    everything_ok = create_shared_memory(sizeof(bool));
    *everything_ok = true;
    current_action_number = create_shared_memory(sizeof(int32_t));
    *current_action_number = 1;
    queue_waiting_number = create_shared_memory(sizeof(u_int32_t) * 3);
    memset(queue_waiting_number, 0, sizeof(u_int32_t) * 3);
    if (office_open == NULL || everything_ok == NULL || current_action_number == NULL || queue_waiting_number == NULL) {
        fprintf(stderr, "Failed to create shared memory\n");
        exit(1);
    }
}

/**
 * Frees the shared memory
 */
void free_shared(void) {
    munmap(office_open, sizeof(bool));
    munmap(everything_ok, sizeof(bool));
    munmap(current_action_number, sizeof(int32_t));
    munmap(queue_waiting_number, sizeof(u_int32_t) * 3);
}

//****************************************** CUSTOMER LOGIC ****************************************************/

/**
 * Returns true if the current process is a customer.
 */
bool is_customer(void) {
    return customer_id != 0;
}

/**
 * Initializes the customers
 */
void init_customers(void) {
    for (u_int32_t id = 1; id <= n_customers; ++id) {
        pid_t pid = fork();
        if (pid == 0) {
            action("Z %d: started", id);
            srandom(time(NULL) * 7 + id * 11 + getpid() * 13);
            customer_id = id;
            return;
        }
        if (pid < 0) {
            debug("Failed to fork customer %d", id);
            open_large_gate();
        }
    }
}

/**
 * The main function for the customer process
 */
void do_customer_stuff(void) {
    u_int32_t customer_wait_time = gen_rand_u_int(0, max_customer_wait_time) * 1000;
    debug("Customer %d waiting for %d", customer_id, customer_wait_time / 1000);
    usleep(customer_wait_time);


    sem_wait(sem_office_open);
    if (!*office_open) {
        sem_post(sem_office_open);
        action("Z %d: going home", customer_id);
        return;
    }
    u_int32_t service_number = gen_rand_u_int(1, 3);
    action("Z %d: entering office for a service %d", customer_id, service_number);
    sem_post(sem_office_open);

    sem_wait(sem_queue_waiting_number);
    ++queue_waiting_number[service_number - 1];
    sem_post(sem_queue_waiting_number);


    sem_wait(sems_queue[service_number - 1]);

    action("Z %d: called by office worker", customer_id);

    usleep(gen_rand_u_int(1, 10) * 1000);
    action("Z %d: going home", customer_id);
}

//****************************************** OFFICIAL LOGIC ****************************************************/

/**
 * Returns true if the current process is an official.
 */
bool is_official(void) {
    return official_id != 0;
}

/**
 * Initializes the officials
 */
void init_officials(void) {
    for (u_int32_t id = 1; id <= n_officials; ++id) {
        pid_t pid = fork();
        if (pid == 0) {
            action("U %d: started", id);
            srandom(time(NULL) * 7 + id * 11 + getpid() * 13);
            official_id = id;
            break;
        }
        if (pid < 0) {
            debug("Failed to fork official %d", id);
            open_large_gate();
        }
    }
}

/**
 * Main function for the official process
 */
void do_official_stuff(void) {
    while (69) {
        sem_wait(sem_queue_waiting_number);
        int32_t random_occupied_queue_index = get_random_occupied_queue_index();

        if (random_occupied_queue_index == -1) {
            sem_wait(sem_office_open);
            if (!*office_open) {
                sem_post(sem_office_open);
                action("U %d: going home", official_id);
                sem_post(sem_queue_waiting_number);
                return;
            }
            action("U %d: taking break", official_id);
            sem_post(sem_office_open);

            sem_post(sem_queue_waiting_number);
            usleep(gen_rand_u_int(0, max_official_break_time) * 1000);
            action("U %d: break finished", official_id);
            continue;
        }

        sem_post(sems_queue[random_occupied_queue_index]);
        action("U %d: serving a service of type %d", official_id, random_occupied_queue_index + 1);
        --queue_waiting_number[random_occupied_queue_index];
        sem_post(sem_queue_waiting_number);

        usleep(gen_rand_u_int(0, 10) * 1000);
        action("U %d: service finished", official_id);
    }
}

//****************************************** HELPER FUNCTIONS ****************************************************/

/**
 * Generates a random integer between from (inclusive) and to (inclusive).
 * @param from starting number (inclusive)
 * @param to ending number (inclusive)
 * @returns random integer between from and to
 */
u_int32_t gen_rand_u_int(u_int32_t from, u_int32_t to) {
    return (u_int32_t) random() % (to - from + 1) + from;
}

/**
 * Creates a shared memory buffer of the given size
 * @param size
 * @returns pointer to the shared memory buffer
 */
void *create_shared_memory(size_t size) {
    int protection = PROT_READ | PROT_WRITE;
    int visibility = MAP_SHARED | MAP_ANONYMOUS;
    return mmap(NULL, size, protection, visibility, -1, 0);
}

/**
 * Returns the index of the first occupied queue, or -1 if all queues are empty.
 * @returns random index from 0 to 2
 */
int32_t get_random_occupied_queue_index(void) {
    // create a list of indices corresponding to occupied queues
    int32_t occupied_indices[3] = {0};
    int32_t num_occupied_queues = 0;
    for (int i = 0; i < 3; i++) {
        if (queue_waiting_number[i] > 0) {
            occupied_indices[num_occupied_queues] = i;
            num_occupied_queues++;
        }
    }
    if (num_occupied_queues == 0) {
        return -1;
    }
    return occupied_indices[gen_rand_u_int(0, num_occupied_queues - 1)];
}

/**
 * Safely parses a string into an unsigned integer between min (inclusive) and max (inclusive).
 * @param min the minimum valid value (inclusive) for a PARSING_ERROR_BOUNDS not to be returned
 * @param max the maximum valid value (inclusive) for a PARSING_ERROR_BOUNDS not to be returned
 * @param str the string to parse
 * @returns 0 if the string was parsed successfully, 1 otherwise
 */
parsed_u_int_t safely_parse_u_int32(u_int32_t min, u_int32_t max, char *str) {
    char *endptr;
    parsed_u_int_t output;

    long result = strtol(str, &endptr, 10);
    if (endptr == str || *endptr != '\0') {
        output.error = PARSING_ERROR_INVALID;
        return output;
    }
    if (result < min || result > max) {
        output.error = PARSING_ERROR_BOUNDS;
        return output;
    }
    output.value = (u_int32_t) result;
    output.error = PARSING_ERROR_NONE;

    return output;
}

//************************************************ MAIN LOGIC *******************************************************/

/**
 * Releases all the customers from the queue and sends them home immediately.
 */
void open_large_gate(void) {
    sem_wait(sem_queue_waiting_number);
    everything_ok = false;
    for (u_int32_t i = 0; i < 3; ++i) {
        queue_waiting_number[i] = 0;
    }
    for (u_int32_t i = 0; i < n_customers + n_officials; ++i) {
        for (u_int32_t j = 0; j < 3; ++j) sem_post(sems_queue[j]);
    }
    sem_post(sem_queue_waiting_number);
}

/**
 * Parses the command line arguments.
 * The correct format is $ proj2 NZ NU TZ TU F.
 * @param argc the argument count passed to the main() function
 * @param argv the argument vector passed to the main() function
 */
arg_error_t parse_arguments(u_int32_t argc, char *argv[]) {
    if (argc != 6) {
        return ARG_ERROR_INVALID;
    }

    parsed_u_int_t parsed_customers = safely_parse_u_int32(0, UINT_MAX, argv[1]);
    if (parsed_customers.error != PARSING_ERROR_NONE) {
        return ARG_ERROR_NUM_CUSTOMERS;
    }
    n_customers = parsed_customers.value;

    parsed_u_int_t parsed_officials = safely_parse_u_int32(1, UINT_MAX, argv[2]);
    if (parsed_officials.error != PARSING_ERROR_NONE) {
        return ARG_ERROR_NUM_OFFICIALS;
    }
    n_officials = parsed_officials.value;

    parsed_u_int_t parsed_max_customer_wait_time = safely_parse_u_int32(0, 10000, argv[3]);
    if (parsed_max_customer_wait_time.error != PARSING_ERROR_NONE) {
        return ARG_ERROR_CUSTOMER_WAIT_TIME;
    }
    max_customer_wait_time = parsed_max_customer_wait_time.value;

    parsed_u_int_t parsed_max_official_break_time = safely_parse_u_int32(0, 100, argv[4]);
    if (parsed_max_official_break_time.error != PARSING_ERROR_NONE) {
        return ARG_ERROR_OFFICIAL_BREAK_TIME;
    }
    max_official_break_time = parsed_max_official_break_time.value;

    parsed_u_int_t parsed_max_office_open_time = safely_parse_u_int32(0, 10000, argv[5]);
    if (parsed_max_office_open_time.error != PARSING_ERROR_NONE) {
        return ARG_ERROR_OFFICE_OPEN_TIME;
    }
    max_office_open_time = parsed_max_office_open_time.value;

    debug("Number of customers parsed: %d", n_customers);
    debug("Number of officials parsed: %d", n_officials);
    debug("Maximum customer wait time parsed: %d", max_customer_wait_time);
    debug("Maximum official break time parsed: %d", max_official_break_time);
    debug("Maximum office open time parsed: %d", max_office_open_time);
    return ARG_ERROR_NONE;
}

/**
 * Prints the corresponding message associated with the argument error
 * @param argument_error the argument error
 */
void print_argument_error(arg_error_t argument_error) {
    switch (argument_error) {
        case ARG_ERROR_INVALID:
            fprintf(stderr, "Usage: proj2 NZ NU TZ TU F\n");
            break;
        case ARG_ERROR_NUM_CUSTOMERS:
            fprintf(stderr, "The number of customers must be a positive integer between 1 and %u\n", UINT_MAX);
            break;
        case ARG_ERROR_NUM_OFFICIALS:
            fprintf(stderr, "The number of officials must be a positive integer between 1 and %u\n", UINT_MAX);
            break;
        case ARG_ERROR_CUSTOMER_WAIT_TIME:
            fprintf(stderr, "The maximum customer wait time must be a positive integer between 0 and 10000\n");
            break;
        case ARG_ERROR_OFFICIAL_BREAK_TIME:
            fprintf(stderr, "The maximum official break time must be a positive integer between 0 and 100\n");
            break;
        case ARG_ERROR_OFFICE_OPEN_TIME:
            fprintf(stderr, "The maximum office open time must be a positive integer between 0 and 10000\n");
            break;
        case ARG_ERROR_NONE:
            break;
    }
}

/**
 * Returns true if the current process is the main process.
 */
bool is_main_process(void) {
    return customer_id == 0 && official_id == 0;
}

void initialize(void) {
    output_file = fopen(OUTPUT_FILE_NAME, "w");
    if (output_file == NULL) {
        fprintf(stderr, "Error opening output file\n");
        exit(1);
    }

    init_shared();

    // We need to first initialize the semaphores because in the officials and customers, we need the same semaphores
    // be already available
    if (init_semaphores() == false) {
        free_shared();
        exit(1);
    }

    // We're still in the main process, so we don't have to check
    init_customers();

    // We're in the main process or inside an official process, so we have to check so that all instances of officials
    // run under the main process
    if (is_main_process()) {
        init_officials();
    }
}

/**
 * Cleans up all the memory and semaphores
 */
void cleanup(void) {
    remove_semaphores();
    free_shared();
    fclose(output_file);
}

int main(int argc, char *argv[]) {
    arg_error_t argument_error = parse_arguments(argc, argv);
    if (argument_error != ARG_ERROR_NONE) {
        print_argument_error(argument_error);
        return 1;
    }

    initialize();

    if (is_customer()) {
        do_customer_stuff();
    }

    if (is_official()) {
        do_official_stuff();
    }

    if (!is_main_process()) return 0; // filter out all the rest of the processes

    u_int32_t sleep_time_ms = gen_rand_u_int(max_office_open_time / 2, max_office_open_time) * 1000;
    usleep(sleep_time_ms);

    sem_wait(sem_office_open);
    *office_open = false;
    action("closing");
    sem_post(sem_office_open);

    // Wait for all customers and officials to finish their jobs
    while (wait(NULL) > 0);

    sem_wait(sem_queue_waiting_number); // I reuse this semaphore
    if (!everything_ok) {
        sem_post(sem_queue_waiting_number);
        fprintf(stderr, "Initializing new processes failed.\n");
        cleanup();
        return 1;
    }
    sem_post(sem_queue_waiting_number);

    cleanup();
    return 0;
}
