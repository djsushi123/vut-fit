#ifndef SEMAPHORES_MAIN_H
#define SEMAPHORES_MAIN_H

/**
 * Error returned after parsing the command line arguments
 */
typedef enum {
    ARG_ERROR_NONE,
    ARG_ERROR_INVALID,
    ARG_ERROR_NUM_CUSTOMERS,
    ARG_ERROR_NUM_OFFICIALS,
    ARG_ERROR_OFFICIAL_BREAK_TIME,
    ARG_ERROR_CUSTOMER_WAIT_TIME,
    ARG_ERROR_OFFICE_OPEN_TIME
} arg_error_t;

/**
 * Error returned after parsing an unsigned integer with safely_parse_u_int32
 */
typedef enum {
    PARSING_ERROR_NONE,
    PARSING_ERROR_BOUNDS,
    PARSING_ERROR_INVALID
} parsing_error_t;

/**
 * Parsed unsigned integer result returned by safely_parse_u_int32
 */
typedef struct {
    u_int32_t value;
    parsing_error_t error;
} parsed_u_int_t;

bool init_semaphores(void);

void remove_semaphores(void);

void init_shared(void);

void free_shared(void);

bool is_customer(void);

void init_customers(void);

void do_customer_stuff(void);

bool is_official(void);

void init_officials(void);

void do_official_stuff(void);

u_int32_t gen_rand_u_int(u_int32_t from, u_int32_t to);

void *create_shared_memory(size_t size);

int32_t get_random_occupied_queue_index(void);

parsed_u_int_t safely_parse_u_int32(u_int32_t min, u_int32_t max, char *str);

void open_large_gate(void);

arg_error_t parse_arguments(u_int32_t argc, char *argv[]);

void print_argument_error(arg_error_t argument_error);

bool is_main_process(void);

void initialize(void);

void cleanup(void);

#endif // SEMAPHORES_MAIN_H
