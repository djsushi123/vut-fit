#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>

sem_t *semaphore;

int main(void) {

    sem_unlink("SEM_COOL");
    semaphore = sem_open("SEM_COOL", O_CREAT | O_EXCL, 0666, 1);

    pid_t pid = fork();

    sem_wait(semaphore);
    if (pid == 0) {
        printf("Hello from child!\n");
    } else {
        printf("Hello from parent!\n");
    }
    usleep(1000000);
    sem_post(semaphore);

    if (pid == 0) {
        puts("Child is done.");
        return 0;
    }

    usleep(3000000);

    sem_close(semaphore);
    return 0;
}
