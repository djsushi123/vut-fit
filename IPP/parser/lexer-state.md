```puml
state "S" as start

[*] --> start

start --> A : a-z, A-Z
A --> [*]

start --> B : #
B --> B : Σ - {NL}
B --> [*] : don't return any tokens

start --> C : NL
C --> C : NL
C --> [*] : return one NL token

legend top left
    | Symbol | Explanation |
    | NL | Newline |
endlegend
```