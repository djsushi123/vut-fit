import os
import re
import sys
from dataclasses import dataclass
from enum import Enum
from sys import stdin
from xml.etree.ElementTree import Element, tostring, indent, SubElement

DEBUG = os.environ.get('DEBUG', '0') == '1'


class ErrorCode(Enum):
    """
    Represents an error code that gets returned by the parser.
    """
    WRONG_ARGUMENTS = 10
    MISSING_HEADER = 21
    BAD_OPCODE = 22
    OTHER = 23
    INTERNAL = 99


class TemplateOperandType(Enum):
    SYMBOL = 'symbol'
    LABEL = 'label'
    TYPE = 'type'
    VAR = 'var'


class OperandType(Enum):
    INT = 'int'
    BOOL = 'bool'
    STRING = 'string'
    NIL = 'nil'
    LABEL = 'label'
    TYPE = 'type'
    VAR = 'var'


@dataclass
class Operand:
    value: str
    type: OperandType


def exit_with_error(error_code: ErrorCode, message: str):
    if DEBUG:
        print(message)
    exit(error_code.value)


REGEXES = {
    OperandType.INT: re.compile(r'int@([+-]?(?:\d*|0o[0-7]+|0x[\da-fA-F]+))'),
    OperandType.BOOL: re.compile(r'bool@(true|false)'),
    OperandType.STRING: re.compile(r'string@((?:[^\\]|\\\d{3})*)'),
    OperandType.NIL: re.compile(r'nil@(nil)'),
    OperandType.LABEL: re.compile(r'([A-Za-z_\-$&%*!?][\w\-$&%*!?]*)'),
    OperandType.TYPE: re.compile(r'(int|bool|string)'),
    OperandType.VAR: re.compile(r'((?:GF|LF|TF)@[A-Za-z_\-$&%*!?][\w\-$&%*!?]*)')
}


def parse_operand(operand: str, operand_type: TemplateOperandType) -> Operand:
    if operand_type == TemplateOperandType.SYMBOL:
        for operand_type in [OperandType.INT, OperandType.BOOL, OperandType.STRING, OperandType.NIL, OperandType.VAR]:
            match = REGEXES[operand_type].fullmatch(operand)
            if match is not None:
                return Operand(match.group(1), operand_type)
        exit_with_error(ErrorCode.OTHER, f"Invalid operand {operand} of type {operand_type}")

    operand_to_check = None
    if operand_type == TemplateOperandType.LABEL:
        operand_to_check = OperandType.LABEL
    elif operand_type == TemplateOperandType.TYPE:
        operand_to_check = OperandType.TYPE
    elif operand_type == TemplateOperandType.VAR:
        operand_to_check = OperandType.VAR

    match = REGEXES[operand_to_check].fullmatch(operand)
    if match is None:
        exit_with_error(ErrorCode.OTHER, f"Invalid operand {operand} of type {operand_type}")
    return Operand(match.group(1), operand_type)


@dataclass
class Instruction:
    name: str
    operands: list[Operand]


TOT = TemplateOperandType
TEMPLATES = {
    'MOVE': [TOT.VAR, TOT.SYMBOL],
    'CREATEFRAME': [],
    'PUSHFRAME': [],
    'POPFRAME': [],
    'DEFVAR': [TOT.VAR],
    'CALL': [TOT.LABEL],
    'RETURN': [],
    'PUSHS': [TOT.SYMBOL],
    'POPS': [TOT.VAR],
    'ADD': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'SUB': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'MUL': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'IDIV': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'LT': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'GT': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'EQ': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'AND': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'OR': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'NOT': [TOT.VAR, TOT.SYMBOL],
    'INT2CHAR': [TOT.VAR, TOT.SYMBOL],
    'STRI2INT': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'READ': [TOT.VAR, TOT.TYPE],
    'WRITE': [TOT.SYMBOL],
    'CONCAT': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'STRLEN': [TOT.VAR, TOT.SYMBOL],
    'GETCHAR': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'SETCHAR': [TOT.VAR, TOT.SYMBOL, TOT.SYMBOL],
    'TYPE': [TOT.VAR, TOT.SYMBOL],
    'LABEL': [TOT.LABEL],
    'JUMP': [TOT.LABEL],
    'JUMPIFEQ': [TOT.LABEL, TOT.SYMBOL, TOT.SYMBOL],
    'JUMPIFNEQ': [TOT.LABEL, TOT.SYMBOL, TOT.SYMBOL],
    'EXIT': [TOT.SYMBOL],
}


def check_heading(lines: list[str]):
    if len(lines) == 0:
        exit_with_error(ErrorCode.MISSING_HEADER, "File empty")
    if lines[0] != '.IPPcode24':
        exit_with_error(ErrorCode.MISSING_HEADER, "Header missing")


def parse_instruction(operation: list[str]) -> Instruction:
    opcode = operation[0]
    if opcode == '.IPPcode24':
        exit_with_error(ErrorCode.OTHER, "Second header detected")
    opcode = opcode.upper()
    operand_count = len(operation) - 1

    match = TEMPLATES.get(opcode, None)
    if match is None:
        exit_with_error(ErrorCode.BAD_OPCODE, f"Invalid OPCODE: {opcode}")

    if operand_count != len(match):
        exit_with_error(ErrorCode.OTHER,
                        f"Invalid operand count for {opcode}. Expected {len(match)} got {operand_count}")

    operands = []
    for index, operand_type in enumerate(match):
        operands.append(parse_operand(operation[index + 1], operand_type))

    return Instruction(opcode, operands)


def pretty_print(element: Element):
    indent(element, space='  ', level=0)
    print(str(tostring(element, encoding='utf-8').decode('utf-8')))


def clean_line(line: str) -> str:
    return line.split('#')[0].strip()


HELP_TEXT = "This program transpiles IPP24code instructions into XML. Use --help to show help."


if __name__ == '__main__':
    try:
        first_arg = sys.argv[1]
        if first_arg == '--help':
            print(HELP_TEXT)
            exit(0)
    except IndexError:
        pass
    lines = stdin.read().splitlines()
    lines = list(map(clean_line, lines))
    lines = list(filter(lambda line: line != '' and not line.isspace(), lines))
    check_heading(lines)
    lines = lines[1:]
    root = Element('program')
    root.attrib['language'] = 'IPPcode24'
    instructions = [parse_instruction(line.split()) for line in lines]
    for order, instruction in enumerate(instructions):
        child = SubElement(root, 'instruction')
        child.attrib['order'] = str(order + 1)
        child.attrib['opcode'] = instruction.name
        for index, operand in enumerate(instruction.operands):
            arg = SubElement(child, f'arg{index + 1}')
            arg.text = operand.value
            arg.attrib['type'] = operand.type.value

    if DEBUG:
        pretty_print(root)
    else:
        pretty_print(root)
