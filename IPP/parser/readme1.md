# IPP-24 `parser.py` Documentation

`parse.py` is a Python script that transpiles IPP24code programs into a more machine-readable XML format.

## Usage

There is only one file - the Python script `parse.py`. This script is intended to run using Python `3.10.11`. This file
is run sing the command `python parse.py` or, if you have more Python versions installed, using `python3.10 parse.py`.
The script reads `stdin` until it finds the first EOF. Then, it terminates reading and considers the input to be over.
The script then outputs the converted XML into `stdout`.

## Debugging Mode

Debugging mode can be enabled by setting the DEBUG environment variable to '1':

```shell
DEBUG=1 python script.py < input.ipp > output.xml
```

## Code Documentation

The code is organized into a couple of `data class`es and functions. The `@dataclass` Python annotation is used because
it simplifies a lot of code and reduces duplicated code.

### Data Classes

- `Operand`: Represents an operand with a value and type.
- `Instruction`: Represents an instruction with a name and a list of operands.

### Functions

- `exit_with_error(error_code: ErrorCode, message: str)`: Exits the script with an error code and optional debug
  message.
- `parse_operand(operand: str, operand_type: TemplateOperandType) -> Operand`: Parses an operand based on its type.
- `check_heading(lines: list[str])`: Checks if the input file has the correct header.
- `parse_instruction(operation: list[str]) -> Instruction`: Parses an instruction from a list of operation tokens.
- `pretty_print(element: Element)`: Prints an XML element with indentation.
- `clean_line(line: str) -> str`: Removes comments and whitespace from a line.

