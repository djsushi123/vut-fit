# Preference and tcl based customizations can be placed in this file.  This file will get sourced
# each time the default preferences are loaded and this happens whenever a transition occurs, such as loading
# or unloading a simulation or design.

# Here is a common example, this preference sets the license idle timeout default value. You can uncomment this line and change the default value.
# Changing "-value 1200" to "-value 2400" while result in a new default value of 2400 (40 minutes).  All users that reference this release tree will
# inherit the new default value.

#pref::set -type int -category General -name LicenseIdleTimeout -label {Idle timeout for license checkback} -value 1200 -minvalue 900 -maxvalue 10000 -hide -description {Indicates the idle timeout delay and license check back, in seconds.  A value of 900 means no checkback.}
