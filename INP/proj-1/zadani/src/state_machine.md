# State machine

```mermaid
stateDiagram-v2
    [*] --> START: EN==0
    START --> INIT: EN==1

    state INIT {
        [*] --> INIT_S0
        state "PTR_PC_SEL=0" as INIT_S0

        INIT_S0 --> [*]
    }

```