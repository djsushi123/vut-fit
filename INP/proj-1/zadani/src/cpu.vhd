-- cpu.vhd: Simple 8-bit CPU (BrainFuck interpreter)
-- Copyright (C) 2023 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): jmeno <login AT stud.fit.vutbr.cz>
--
package types_pkg is
  type instruction_type is (
      INSTR_INC_PTR, INSTR_DEC_PTR, INSTR_INC_MEM, INSTR_DEC_MEM, INSTR_JMP_IF_ZERO,
      INSTR_JMP_IF_NOT_ZERO, INSTR_BREAK, INSTR_WRITE, INSTR_READ, INSTR_RETURN, INSTR_NOP,
      INSTR_ZERO
    );
end package;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;
  use work.types_pkg.all;

entity cpu is
  port (
    CLK        : in  std_logic;                     -- hodinovy signal
    RESET      : in  std_logic;                     -- asynchronni reset procesoru
    EN         : in  std_logic;                     -- povoleni cinnosti procesoru

    -- synchronni pamet RAM
    DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
    DATA_WDATA : out std_logic_vector(7 downto 0);  -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
    DATA_RDATA : in  std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
    DATA_RDWR  : out std_logic;                     -- cteni (0) / zapis (1)
    DATA_EN    : out std_logic;                     -- povoleni cinnosti

    -- vstupni port
    IN_DATA    : in  std_logic_vector(7 downto 0);  -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
    IN_VLD     : in  std_logic;                     -- data platna
    IN_REQ     : out std_logic;                     -- pozadavek na vstup data

    -- vystupni port
    OUT_DATA   : out std_logic_vector(7 downto 0);  -- zapisovana data
    OUT_BUSY   : in  std_logic;                     -- LCD je zaneprazdnen (1), nelze zapisovat
    OUT_WE     : out std_logic;                     -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'

    -- stavove signaly
    READY      : out std_logic;                     -- hodnota 1 znamena, ze byl procesor inicializovan a zacina vykonavat program
    DONE       : out std_logic                      -- hodnota 1 znamena, ze procesor ukoncil vykonavani programu (narazil na instrukci halt)
  );
end entity;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;
  use work.types_pkg.all;

entity counter is
  port (
    CLK    : in  std_logic;
    RESET  : in  std_logic;
    INC    : in  std_logic;
    DEC    : in  std_logic;
    OUTPUT : out std_logic_vector(12 downto 0)
  );
end entity;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;
  use work.types_pkg.all;

entity mux2 is
  port (
    IN0, IN1 : in  std_logic_vector(12 downto 0);
    SEL      : in  std_logic;
    OUTPUT   : out std_logic_vector(12 downto 0)
  );
end entity;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;
  use work.types_pkg.all;

entity mux4 is
  port (
    IN0, IN1, IN2, IN3 : in  std_logic_vector(7 downto 0);
    SEL                : in  std_logic_vector(1 downto 0);
    OUTPUT             : out std_logic_vector(7 downto 0)
  );
end entity;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;
  use work.types_pkg.all;

entity fsm is
  port (
    EN, CLK, RESET     : in  std_logic;
    DONE, READY        : out std_logic;

    -- I/O
    IN_REQ, OUT_WE     : out std_logic;
    IN_VLD, OUT_BUSY   : in  std_logic;

    -- RAM control
    DATA_RDWR, DATA_EN : out std_logic;
    PTR_PC_SEL         : out std_logic;
    WDATA_SEL          : out std_logic_vector(1 downto 0);

    -- bracket counter
    CNT_INC, CNT_DEC   : out std_logic;
    CNT_RESET          : out std_logic;
    CNT_IS_ZERO        : in  std_logic;

    -- data pointer into the RAM
    PTR_INC, PTR_DEC   : out std_logic;
    PTR_RESET          : out std_logic;

    -- program counter
    PC_INC, PC_DEC     : out std_logic;
    PC_RESET           : out std_logic;
    INSTRUCTION        : in  instruction_type
  );
end entity;
-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------

architecture behavioral of counter is
  signal VALUE : std_logic_vector(12 downto 0) := (others => '0');
begin
  compute: process (CLK, RESET)
  begin
    if RESET = '1' then
      VALUE <= (others => '0');
    elsif rising_edge(CLK) then
      if INC = '1' then
        VALUE <= VALUE + 1;
      elsif DEC = '1' then
        VALUE <= VALUE - 1;
      end if;

    end if;
  end process;

  OUTPUT <= VALUE;
end architecture;

architecture dataflow of mux2 is
begin
  process (SEL, IN0, IN1)
  begin
    if SEL = '0' then
      output <= IN0;
    else
      output <= IN1;
    end if;
  end process;
end architecture;

architecture dataflow of mux4 is
begin
  process (SEL, IN0, IN1, IN2, IN3)
  begin
    case SEL is
      when "00" => OUTPUT <= IN0;
      when "01" => OUTPUT <= IN1;
      when "10" => OUTPUT <= IN2;
      when others => OUTPUT <= IN3;
    end case;
  end process;
end architecture;

-- ===== FSM IMPL =====

architecture behavioral of fsm is
  type FSMstate is (
      S_START, S_EXECUTE, S_LOAD_INSTR, S_SKIP_INSTR, -- special states
      S_INIT_0, S_INIT_1, S_INIT_2, -- initialization states
      S_MOVE_RIGHT_0, S_MOVE_RIGHT_1, -- pointer increment states
      S_MOVE_LEFT_0, S_MOVE_LEFT_1, -- pointer decrement states
      S_INCREMENT_0, S_INCREMENT_1, S_INCREMENT_2, -- memory increment states
      S_DECREMENT_0, S_DECREMENT_1, S_DECREMENT_2, -- memory decrement states
      S_WRITE_0, S_WRITE_1, S_WRITE_2, S_WRITE_3, -- write to the LCD states
      S_READ_0, S_READ_1, S_READ_2, -- read from the "standard input"
      S_LBRACE_0, S_LBRACE_1, S_LBRACE_2, S_LBRACE_3, S_LBRACE_4, S_LBRACE_5, S_LBRACE_6_0, S_LBRACE_6_1, S_LBRACE_7, -- '[' handling states
      S_RBRACE_0, S_RBRACE_1, S_RBRACE_1_0, S_RBRACE_2, S_RBRACE_3, S_RBRACE_4, S_RBRACE_5,
      S_RBRACE_6_0, S_RBRACE_6_1, S_RBRACE_7, S_RBRACE_8_0, S_RBRACE_8_1, -- ']' handling states
      S_BREAK_0, S_BREAK_00, S_BREAK_1, S_BREAK_2, S_BREAK_3_0, S_BREAK_3_1, S_BREAK_4, -- '~' handling states 
      S_RETURN_0
    );
  signal pstate : FSMstate;
  signal nstate : FSMstate;
begin
  pstatereg: process (CLK, RESET)
  begin
    if RESET = '1' then
      pstate <= S_START;
    elsif rising_edge(CLK) then
      pstate <= nstate;
    end if;
  end process;

  output_logic: process (pstate) -- , other stuff
  begin
    case pstate is
      when S_START => -- initial state
        READY <= '0';
        DONE <= '0';
        DATA_EN <= '0';
        IN_REQ <= '0';
        OUT_WE <= '0';
        CNT_INC <= '0';
        CNT_DEC <= '0';
        CNT_RESET <= '1';
        PTR_INC <= '0';
        PTR_DEC <= '0';
        PTR_RESET <= '1';
        PC_INC <= '0';
        PC_DEC <= '0';
        PC_RESET <= '1';

      when S_INIT_0 => -- 0. initialization state; preparing to read data from RAM 
        PTR_PC_SEL <= '0';
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        PTR_INC <= '0';
        CNT_RESET <= '0';
        PTR_RESET <= '0';
        PC_RESET <= '0';
      when S_INIT_1 => -- 1. initialization state; reading RAM and deciding on whether it's a '@' or not
        DATA_EN <= '0';
        PTR_INC <= '1';
      when S_INIT_2 => -- 2. initialization state; setting the READY flag and stopping PTR incrementing; reading first instruction at PC
        PTR_INC <= '0';
        DATA_EN <= '1';
        PTR_PC_SEL <= '1'; -- select PC to read the program instruction now
        READY <= '1';

      when S_EXECUTE => -- The main executing state; All specific instruction executing states return to this state once finished
        -- This state resets and disables everything (it serves as an autoreset between instruction handling states)
        DATA_EN <= '0';
        PTR_INC <= '0';
        PC_INC <= '0';
      when S_LOAD_INSTR => -- Loads the next instruction and delegates the DATA_EN disabling to S_EXECUTE. Make sure to PC_INC before this instruction
        DATA_EN <= '1';
        PTR_PC_SEL <= '1';
        DATA_RDWR <= '0';
        PC_INC <= '0';
      when S_SKIP_INSTR => -- just sets the PC_INC to 1 in order to increment PC and then immediately transitions to S_LOAD_INSTR
        PC_INC <= '1';

      when S_MOVE_RIGHT_0 => -- 0. right movement state; sets PTR_INC to 1
        PTR_INC <= '1';
      when S_MOVE_RIGHT_1 => -- 1. right movement state; stops incrementing PTR and sets PC_INC to 1 in order to read the next instruction
        PTR_INC <= '0';
        PC_INC <= '1';

      when S_MOVE_LEFT_0 => -- 0. left movement state; sets PTR_DEC to 1
        PTR_DEC <= '1';
      when S_MOVE_LEFT_1 => -- 1. left movement state; stops decrementing PTR and sets PC_INC to 1 in order to read the next instruction
        PTR_DEC <= '0';
        PC_INC <= '1';

      when S_INCREMENT_0 => -- 0. mem increment state; reads the data from RAM at PTR and saves the same data +1 using the mux2
        DATA_EN <= '1';
        DATA_RDWR <= '0'; -- reading
        PTR_PC_SEL <= '0'; -- selecting PTR
      when S_INCREMENT_1 => -- 1. mem increment state; writes the data +1 back to the RAM while selecting "10" on the mux4
        DATA_RDWR <= '1'; -- writing
        WDATA_SEL <= "10"; -- writing value +1
        PC_INC <= '1'; -- don't forget to increment the program counter, since we just read an instruction
      when S_INCREMENT_2 => -- 2. mem increment state; reads the next instruction
        DATA_RDWR <= '0'; -- reading
        PTR_PC_SEL <= '1'; -- select PC
        PC_INC <= '0'; -- stop incrementing PC

      when S_DECREMENT_0 => -- 0. mem increment state; reads the data from RAM at PTR and saves the same data +1 using the mux2
        DATA_EN <= '1';
        DATA_RDWR <= '0'; -- reading
        PTR_PC_SEL <= '0'; -- selecting PTR
      when S_DECREMENT_1 => -- 1. mem increment state; writes the data +1 back to the RAM while selecting "10" on the mux4
        DATA_RDWR <= '1'; -- writing
        WDATA_SEL <= "01"; -- writing value +1
        PC_INC <= '1'; -- don't forget to increment the program counter, since we just read an instruction
      when S_DECREMENT_2 => -- 2. mem increment state; reads the next instruction
        DATA_RDWR <= '0'; -- reading
        PTR_PC_SEL <= '1'; -- select PC
        PC_INC <= '0'; -- stop incrementing PC

      when S_WRITE_0 => -- 0. LCD write state; reads the RAM for the thing to write
        PTR_PC_SEL <= '0';
        DATA_EN <= '1';
        DATA_RDWR <= '0';
      when S_WRITE_1 => -- 1. LCD write state; cycles into itself while waiting for OUT_BUSY == 0, also disables RAM reading
        DATA_EN <= '0';
      when S_WRITE_2 => -- 2. LCD write state; starts writing onto the LCD
        OUT_WE <= '1';
      when S_WRITE_3 => -- 3. LCD write state; disables writing to OUT_DATA and sets PC_INC to 1 for instruction retrieval
        OUT_WE <= '0';
        PC_INC <= '1';

      when S_READ_0 => -- 0. read state; requests input data and then waits until IN_VLD is 1, then proceeds to the next state
        IN_REQ <= '1';
      when S_READ_1 => -- 1. read state; starts inserting the input data from IN_DATA to the RAM
        WDATA_SEL <= "00"; -- in order to insert the data coming from I/O
        PTR_PC_SEL <= '0'; -- select PTR in order to save to mem[PTR]
        DATA_RDWR <= '1'; -- write
        DATA_EN <= '1';
      when S_READ_2 => -- 2. read state; ends writing to RAM and prepares for next instruction read
        DATA_EN <= '0';
        IN_REQ <= '0'; -- we're done writing the data to RAM, so we stop requesting the data
        PC_INC <= '1';

      when S_LBRACE_0 => -- 0. lbrace state; increments PC -> PC + 1 and reads from mem[PTR], prepares CNT
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        PTR_PC_SEL <= '0';
        PC_INC <= '1';
        CNT_RESET <= '1';
      when S_LBRACE_1 => -- 1. lbrace state; stops RAM reading, stops CNT reset, stops PC from incrementing further
        DATA_EN <= '0';
        CNT_RESET <= '0';
        PC_INC <= '0';
      when S_LBRACE_2 => -- 2. lbrace state; starts incrementing CNT by 1, this is done only once
        CNT_INC <= '1';
      when S_LBRACE_3 => -- 3. lbrace state; start of the while loop (CNT != 0); stops PC from incrementing (from the end of the loop)
        CNT_INC <= '0';
        PC_INC <= '0';
      when S_LBRACE_4 => -- 4. lbrace state; starts reading rada from mem[PC]
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        PTR_PC_SEL <= '1'; -- reading from PC
      when S_LBRACE_5 => -- 5. lbrace state; ends reading from mem[PC], branches into decrementing or incrementing CNT
        DATA_EN <= '0';
      when S_LBRACE_6_0 => -- 6.0. lbrace state; if we encountered a '[', we increase CNT
        CNT_INC <= '1';
      when S_LBRACE_6_1 => -- 6.1. lbrace state; if we encountered a ']', we decrease CNT
        CNT_DEC <= '1';
      when S_LBRACE_7 => -- 7. lbrace state; we stop increasing/decreasing of CNT and go back to the conditioned state at the beginning of the while
        -- also starts increasing PC. The increasing is stopped at the top of the loop  
        CNT_DEC <= '0';
        CNT_INC <= '0';
        PC_INC <= '1';

      when S_RBRACE_0 => -- 0. rbrace state; start reading the RAM at PTR, start resetting CNT
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        PTR_PC_SEL <= '0';
        CNT_RESET <= '1';
      when S_RBRACE_1 => -- 1. rbrace state; stop resetting CNT, stop reading RAM, decide
        DATA_EN <= '0';
        CNT_RESET <= '0';
      when S_RBRACE_1_0 => -- 1.0. rbrace state; side state to start increasing PC in order to load next instruction using LOAD_INSTR
        PC_INC <= '1';
      when S_RBRACE_2 => -- 2. rbrace state; start increasing CNT to increase it to the value of 1, start decreasing PC
        CNT_INC <= '1';
        PC_DEC <= '1';
      when S_RBRACE_3 => -- 3. rbrace state; while top, stop CNT at 1 and stop decreasing PC because we're going to read mem through it
        CNT_INC <= '0';
        PC_DEC <= '0';
        PC_INC <= '0';
        --CNT_DEC <= '0';
      when S_RBRACE_4 => -- 4. rbrace state; start reading mem[PC]
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        PTR_PC_SEL <= '1'; -- select PC
      when S_RBRACE_5 => -- 5. rbrace state; stop reading mem[PC] and branch off
        DATA_EN <= '0';
      when S_RBRACE_6_0 => -- 6.0. rbrace state; increase CNT because we found ']'
        CNT_INC <= '1';
      when S_RBRACE_6_1 => -- 6.1. rbrace state; decrease CNT because we found '['
        CNT_DEC <= '1';
      when S_RBRACE_7 => -- 7. rbrace state; stop increasing/decreasing CNT
        CNT_INC <= '0';
        CNT_DEC <= '0';
      when S_RBRACE_8_0 => -- 8.0. rbrace state; start decreasing PC and continue loop
        PC_DEC <= '1'; -- continue loop
      when S_RBRACE_8_1 => -- 8.1. rbrace state; start increasing PC and go back to loop start; leaving the loop because we found the matching '['
        PC_INC <= '1'; -- prepare for LOAD_INSTR

      when S_BREAK_0 => -- 0. break state; 
        PC_INC <= '1';
        CNT_RESET <= '1';
      when S_BREAK_00 => -- 0.0. break state;
        PC_INC <= '0';
        CNT_RESET <= '0';
        CNT_INC <= '1';
      when S_BREAK_1 => -- 1. break state;
        CNT_INC <= '0';
        DATA_EN <= '1';
        DATA_RDWR <= '0';
        PC_INC <= '0';
        PTR_PC_SEL <= '1'; -- PC select
      when S_BREAK_2 => -- 2. break state;
        DATA_EN <= '0';
      when S_BREAK_3_0 => -- 3.0. break state;
        CNT_INC <= '1';
      when S_BREAK_3_1 => -- 3.1. break state;
        CNT_DEC <= '1';
      when S_BREAK_4 => -- 4. break state;
        CNT_INC <= '0';
        CNT_DEC <= '0';
        PC_INC <= '1';

      when S_RETURN_0 => -- Void state which gets stuck in itself once the program ends
        DONE <= '1';
      when others =>
    end case;
  end process;

  nstate_logic: process (pstate, INSTRUCTION, EN, OUT_BUSY, IN_VLD, CNT_IS_ZERO) -- , other stuff that the nstate depends on
  begin
    nstate <= S_START;
    case pstate is
      when S_START =>
        if EN = '1' then
          nstate <= S_INIT_0;
        end if;

      when S_INIT_0 => nstate <= S_INIT_1;
      when S_INIT_1 =>
        if INSTRUCTION = INSTR_RETURN then
          nstate <= S_INIT_2;
        else
          nstate <= S_INIT_0;
        end if;
      when S_INIT_2 => nstate <= S_EXECUTE;

      when S_EXECUTE =>
        case INSTRUCTION is
          when INSTR_INC_PTR => nstate <= S_MOVE_RIGHT_0;
          when INSTR_DEC_PTR => nstate <= S_MOVE_LEFT_0;
          when INSTR_INC_MEM => nstate <= S_INCREMENT_0;
          when INSTR_DEC_MEM => nstate <= S_DECREMENT_0;
          when INSTR_JMP_IF_ZERO => nstate <= S_LBRACE_0;
          when INSTR_JMP_IF_NOT_ZERO => nstate <= S_RBRACE_0;
          when INSTR_BREAK => nstate <= S_BREAK_0;
          when INSTR_WRITE => nstate <= S_WRITE_0;
          when INSTR_READ => nstate <= S_READ_0;
          when INSTR_RETURN => nstate <= S_RETURN_0;
          when INSTR_ZERO => -- nothing
          when INSTR_NOP => nstate <= S_SKIP_INSTR;
        end case;
      when S_LOAD_INSTR => nstate <= S_EXECUTE;
      when S_SKIP_INSTR => nstate <= S_LOAD_INSTR;

      when S_MOVE_RIGHT_0 => nstate <= S_MOVE_RIGHT_1;
      when S_MOVE_RIGHT_1 => nstate <= S_LOAD_INSTR;

      when S_MOVE_LEFT_0 => nstate <= S_MOVE_LEFT_1;
      when S_MOVE_LEFT_1 => nstate <= S_LOAD_INSTR;

      when S_INCREMENT_0 => nstate <= S_INCREMENT_1;
      when S_INCREMENT_1 => nstate <= S_INCREMENT_2;
      when S_INCREMENT_2 => nstate <= S_EXECUTE;

      when S_DECREMENT_0 => nstate <= S_DECREMENT_1;
      when S_DECREMENT_1 => nstate <= S_DECREMENT_2;
      when S_DECREMENT_2 => nstate <= S_EXECUTE;

      when S_WRITE_0 => nstate <= S_WRITE_1;
      when S_WRITE_1 =>
        if OUT_BUSY = '0' then
          nstate <= S_WRITE_2;
        else
          nstate <= S_WRITE_1; -- shouldn't be necessary; check in the end
        end if;
      when S_WRITE_2 => nstate <= S_WRITE_3;
      when S_WRITE_3 => nstate <= S_LOAD_INSTR;

      when S_READ_0 =>
        if IN_VLD = '1' then
          nstate <= S_READ_1;
        else
          nstate <= S_READ_0; -- again, I'm not sure if this is necessary
        end if;
      when S_READ_1 => nstate <= S_READ_2;
      when S_READ_2 => nstate <= S_LOAD_INSTR;

      when S_LBRACE_0 => nstate <= S_LBRACE_1;
      when S_LBRACE_1 =>
        if INSTRUCTION = INSTR_ZERO then
          nstate <= S_LBRACE_2;
        else
          nstate <= S_LOAD_INSTR;
        end if;
      when S_LBRACE_2 => nstate <= S_LBRACE_3;
      when S_LBRACE_3 =>
        if CNT_IS_ZERO = '1' then
          nstate <= S_LOAD_INSTR;
        else
          nstate <= S_LBRACE_4;
        end if;
      when S_LBRACE_4 => nstate <= S_LBRACE_5;
      when S_LBRACE_5 =>
        if INSTRUCTION = INSTR_JMP_IF_ZERO then
          nstate <= S_LBRACE_6_0;
        elsif INSTRUCTION = INSTR_JMP_IF_NOT_ZERO then
          nstate <= S_LBRACE_6_1;
        else
          nstate <= S_LBRACE_7;
        end if;
      when S_LBRACE_6_0 => nstate <= S_LBRACE_7;
      when S_LBRACE_6_1 => nstate <= S_LBRACE_7;
      when S_LBRACE_7 => nstate <= S_LBRACE_3;

      when S_RBRACE_0 => nstate <= S_RBRACE_1;
      when S_RBRACE_1 =>
        if INSTRUCTION = INSTR_ZERO then
          nstate <= S_RBRACE_1_0;
        else
          nstate <= S_RBRACE_2;
        end if;
      when S_RBRACE_1_0 => nstate <= S_LOAD_INSTR;
      when S_RBRACE_2 => nstate <= S_RBRACE_3;
      when S_RBRACE_3 =>
        if CNT_IS_ZERO = '1' then
          nstate <= S_LOAD_INSTR;
        else
          nstate <= S_RBRACE_4;
        end if;
      when S_RBRACE_4 => nstate <= S_RBRACE_5;
      when S_RBRACE_5 =>
        if INSTRUCTION = INSTR_JMP_IF_ZERO then
          nstate <= S_RBRACE_6_1;
        elsif INSTRUCTION = INSTR_JMP_IF_NOT_ZERO then
          nstate <= S_RBRACE_6_0;
        else
          nstate <= S_RBRACE_7;
        end if;
      when S_RBRACE_6_0 => nstate <= S_RBRACE_7;
      when S_RBRACE_6_1 => nstate <= S_RBRACE_7;
      when S_RBRACE_7 =>
        if CNT_IS_ZERO = '1' then
          nstate <= S_RBRACE_8_1;
        else
          nstate <= S_RBRACE_8_0;
        end if;
      when S_RBRACE_8_0 => nstate <= S_RBRACE_3;
      when S_RBRACE_8_1 => nstate <= S_RBRACE_3;

      when S_BREAK_0 => nstate <= S_BREAK_00;
      when S_BREAK_00 => nstate <= S_BREAK_1;
      when S_BREAK_1 =>
        if CNT_IS_ZERO = '1' then
          nstate <= S_EXECUTE;
        else
          nstate <= S_BREAK_2;
        end if;
      when S_BREAK_2 =>
        if INSTRUCTION = INSTR_JMP_IF_ZERO then -- '['
          nstate <= S_BREAK_3_0;
        elsif INSTRUCTION = INSTR_JMP_IF_NOT_ZERO then -- ']'
          nstate <= S_BREAK_3_1;
        else
          nstate <= S_BREAK_4;
        end if;
      when S_BREAK_3_0 => nstate <= S_BREAK_4;
      when S_BREAK_3_1 => nstate <= S_BREAK_4;
      when S_BREAK_4 => nstate <= S_BREAK_1;
      
      when others => null;
    end case;
  end process;
end architecture;

-- ===== CPU IMPL =====

architecture behavioral of cpu is
  component counter
    port (
      CLK, RESET, INC, DEC : in  std_logic;
      OUTPUT               : out std_logic_vector(12 downto 0)
    );
  end component;
  component fsm
    port (
      EN, CLK, RESET     : in  std_logic;
      DONE, READY        : out std_logic;

      -- I/O
      IN_REQ, OUT_WE     : out std_logic;
      IN_VLD, OUT_BUSY   : in  std_logic;

      -- RAM control
      DATA_RDWR, DATA_EN : out std_logic;
      PTR_PC_SEL         : out std_logic;
      WDATA_SEL          : out std_logic_vector(1 downto 0);

      -- bracket counter
      CNT_INC, CNT_DEC   : out std_logic;
      CNT_RESET          : out std_logic;
      CNT_IS_ZERO        : in  std_logic;

      -- data pointer into the RAM
      PTR_INC, PTR_DEC   : out std_logic;
      PTR_RESET          : out std_logic;

      -- program counter
      PC_INC, PC_DEC     : out std_logic;
      PC_RESET           : out std_logic;
      INSTRUCTION        : in  instruction_type
    );
  end component;
  component mux2
    port (
      IN0, IN1 : in  std_logic_vector(12 downto 0);
      SEL      : in  std_logic;
      OUTPUT   : out std_logic_vector(12 downto 0)
    );
  end component;
  component mux4
    port (
      IN0, IN1, IN2, IN3 : in  std_logic_vector(7 downto 0);
      SEL                : in  std_logic_vector(1 downto 0);
      OUTPUT             : out std_logic_vector(7 downto 0)
    );
  end component;

  signal CNT_OUTPUT                  : std_logic_vector(12 downto 0);
  signal PTR_INC, PTR_DEC, PTR_RESET : std_logic;
  signal CNT_INC, CNT_DEC, CNT_RESET : std_logic;
  signal CNT_IS_ZERO                 : std_logic;
  signal PTR_OUTPUT                  : std_logic_vector(12 downto 0);
  signal PC_INC, PC_DEC, PC_RESET    : std_logic;
  signal PC_OUTPUT                   : std_logic_vector(12 downto 0);
  signal INSTRUCTION                 : instruction_type;
  signal MUX_PTR_PC_SEL              : std_logic;
  signal MUX_WDATA_SEL               : std_logic_vector(1 downto 0);
  signal DATA_RDATA_MINUS_1          : std_logic_vector(7 downto 0);
  signal DATA_RDATA_PLUS_1           : std_logic_vector(7 downto 0);
begin
  CNT: counter port map (CLK => CLK, RESET => CNT_RESET, INC => CNT_INC, DEC => CNT_DEC, OUTPUT => CNT_OUTPUT);
  PTR: counter port map (CLK => CLK, RESET => PTR_RESET, INC => PTR_INC, DEC => PTR_DEC, OUTPUT => PTR_OUTPUT);
  PC: counter port map (CLK => CLK, RESET => PC_RESET, INC => PC_INC, DEC => PC_DEC, OUTPUT => PC_OUTPUT);
  FinalStateMachine: fsm
    port map (
      CLK => CLK, RESET => RESET, READY => READY, DONE => DONE, EN => EN,
      DATA_EN => DATA_EN, DATA_RDWR => DATA_RDWR, WDATA_SEL => MUX_WDATA_SEL, PTR_PC_SEL => MUX_PTR_PC_SEL, INSTRUCTION => INSTRUCTION,
      PC_DEC => PC_DEC, PC_INC => PC_INC, PC_RESET => PC_RESET,
      PTR_DEC => PTR_DEC, PTR_INC => PTR_INC, PTR_RESET => PTR_RESET,
      CNT_INC => CNT_INC, CNT_DEC => CNT_DEC, CNT_RESET => CNT_RESET, CNT_IS_ZERO => CNT_IS_ZERO,
      IN_VLD => IN_VLD, IN_REQ => IN_REQ, OUT_WE => OUT_WE, OUT_BUSY => OUT_BUSY
    );
  -- the multiplexer that selects PTR or PC for the DATA_ADDR output
  ADDR_MUX: mux2 port map (IN0 => PTR_OUTPUT, IN1 => PC_OUTPUT, SEL => MUX_PTR_PC_SEL, OUTPUT => DATA_ADDR);
  DATA_MUX: mux4 port map (IN0 => IN_DATA, IN1 => DATA_RDATA_MINUS_1, IN2 => DATA_RDATA_PLUS_1, IN3 =>(others => '0'), SEL => MUX_WDATA_SEL, OUTPUT => DATA_WDATA);

  cnt_output_is_zero: process (CNT_OUTPUT)
    -- Converts the CNT_OUTPUT as soon as it changes to a boolean (CNT_IS_ZERO) (1 if it is zero, otherwise 1)
  begin
    if CNT_OUTPUT = 0 then
      CNT_IS_ZERO <= '1';
    else
      CNT_IS_ZERO <= '0';
    end if;
  end process;

  instruction_decoder: process (DATA_RDATA)
  begin
    case DATA_RDATA is
      when X"3E" => INSTRUCTION <= INSTR_INC_PTR;
      when X"3C" => INSTRUCTION <= INSTR_DEC_PTR;
      when X"2B" => INSTRUCTION <= INSTR_INC_MEM;
      when X"2D" => INSTRUCTION <= INSTR_DEC_MEM;
      when X"5B" => INSTRUCTION <= INSTR_JMP_IF_ZERO;
      when X"5D" => INSTRUCTION <= INSTR_JMP_IF_NOT_ZERO;
      when X"7E" => INSTRUCTION <= INSTR_BREAK;
      when X"2E" => INSTRUCTION <= INSTR_WRITE;
      when X"2C" => INSTRUCTION <= INSTR_READ;
      when X"40" => INSTRUCTION <= INSTR_RETURN;
      when X"00" => INSTRUCTION <= INSTR_ZERO; -- not really an instruction but it's the best way to pass down a zero to the FSM
      when others => INSTRUCTION <= INSTR_NOP;
    end case;
  end process;

  data_rdata_subtractor: process (DATA_RDATA)
  begin
    DATA_RDATA_MINUS_1 <= DATA_RDATA - 1;
  end process;

  data_rdata_adder: process (DATA_RDATA)
  begin
    DATA_RDATA_PLUS_1 <= DATA_RDATA + 1;
  end process;

  out_data_updater: process (DATA_RDATA)
    -- this process is responsible for updating the OUT_DATA cpu output with the newest DATA_RDATA value
  begin
    OUT_DATA <= DATA_RDATA;
  end process;
end architecture;
