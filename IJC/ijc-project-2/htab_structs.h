#ifndef HTAB_STRUCTS_H__
#define HTAB_STRUCTS_H__

#include "htab.h"

typedef struct htab_item {
    htab_pair_t pair;
    struct htab_item *next;
} htab_item_t;

struct htab {
    size_t size;
    size_t arr_size;
    htab_item_t *arr_ptr[];
};

#endif // HTAB_STRUCTS_H__
