#include <stdlib.h>
#include <stdio.h>
#include "htab.h"
#include "htab_structs.h"

htab_pair_t *htab_lookup_add(htab_t *t, htab_key_t key) {
    // first check if the item is in the table, if yes then return it
    // otherwise, add it to the table
    printf("Adding item with key: %s\n", key);
    size_t index = htab_hash_function(key) % t->arr_size;
    printf("Adding... The key %s resolved to index %zu\n", key, index);
    htab_item_t *item = t->arr_ptr[index];
    htab_item_t *previous = NULL;
    while (item != NULL) {
        if (strcmp(item->pair.key, key) == 0) {
            printf("Item already in the hashtable. Its value is %d\n", item->pair.value);
            return &item->pair;
        }
        previous = item;
        item = item->next;
    }
    printf("Item not yet in the hashtable, adding it now.\n");
    htab_item_t *new_item = malloc(sizeof(htab_item_t));
    if (new_item == NULL) {
        return NULL;
    }
    // allocate a new string for the key
    char *key_copy = malloc(sizeof(char) * (strlen(key) + 1));
    if (key_copy == NULL) {
        free(new_item);
        return NULL;
    }
    strcpy(key_copy, key);
    new_item->pair.key = key_copy;
    new_item->pair.value = 0;
    new_item->next = NULL;

    // if the item is the first in the linked list
    if (previous == NULL) {
        t->arr_ptr[index] = new_item;
    } else {
        previous->next = new_item;
    }
    ++t->size;
    return &new_item->pair;
}
