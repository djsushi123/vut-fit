#include <stdlib.h>
#include <stdbool.h>
#include "htab.h"
#include "htab_structs.h"

bool htab_erase(htab_t *t, htab_key_t key) {
    size_t index = htab_hash_function(key) % t->arr_size;
    htab_item_t *item = t->arr_ptr[index];
    htab_item_t *prev = NULL;
    while (item != NULL) {
        if (strcmp(item->pair.key, key) == 0) {
            if (prev == NULL) {
                t->arr_ptr[index] = item->next;
            } else {
                prev->next = item->next;
            }
            free((void *) item->pair.key);
            free(item);
            --t->size;
            return true;
        }
        prev = item;
        item = item->next;
    }
    return false;
}
