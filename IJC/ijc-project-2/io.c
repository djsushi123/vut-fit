#include <stdio.h>
#include <ctype.h>

int read_word(char *s, int max, FILE *f) {
    int c;
    // skip leading whitespace
    while ((c = fgetc(f)) != EOF) {
        if (!isspace(c)) {
            break;
        }
    }
    if (c == EOF) {
        return EOF;
    }
    int length = 0;
    do {
        if (length < max - 1) {
            s[length++] = (signed char) c;
        }
        c = fgetc(f);
    } while (c != EOF && !isspace(c));
    s[length] = '\0';
    return length;
}
