#include <stdlib.h>
#include "htab.h"
#include "htab_structs.h"

void htab_clear(htab_t *t) {
    for (size_t i = 0; i < t->arr_size; i++) {
        htab_item_t *item = t->arr_ptr[i];
        while (item != NULL) {
            htab_item_t *next = item->next;
            free((void *) item->pair.key);
            free(item);
            item = next;
        }
        t->arr_ptr[i] = NULL;
    }
    t->size = 0;
}
