#include <stdio.h>
#include "htab.h"
#include "htab_structs.h"

void htab_statistics(const htab_t *t) {
    size_t min = t->arr_size;
    size_t max = 0;
    size_t sum = 0;
    size_t count = 0;
    for (size_t i = 0; i < t->arr_size; i++) {
        htab_item_t *item = t->arr_ptr[i];
        size_t bucket_size = 0;
        while (item != NULL) {
            bucket_size++;
            item = item->next;
        }
        if (bucket_size > max) {
            max = bucket_size;
        }
        if (bucket_size < min) {
            min = bucket_size;
        }
        sum += bucket_size;
        count++;
    }
    printf("Min: %zu", min);
}
