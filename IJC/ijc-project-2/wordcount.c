#include <stdio.h>
#include "htab.h"
#include "io.h"
#include "htab_structs.h"

#define MAX_WORD_LEN 255
#define HTAB_SIZE 100

void print_word_count(htab_pair_t *pair) {
    printf("%s\t%d\n", pair->key, pair->value);
}

int main(void) {
    htab_t *t = htab_init(HTAB_SIZE);
    if (t == NULL) {
        fprintf(stderr, "Error: htab_init() failed. Cannot allocate memory.\n");
        return 1;
    }
    char word[MAX_WORD_LEN];
    while (read_word(word, MAX_WORD_LEN + 1, stdin) != EOF) {
        printf("Reading word: \"%s\"\n", word);
        htab_pair_t *pair = htab_lookup_add(t, word);
        if (pair == NULL) {
            fprintf(stderr, "Error: htab_lookup_add() failed. Cannot allocate memory.\n");
            htab_free(t);
            return 1;
        }
        ++pair->value;
        memset(word, 0, sizeof(word));
    }

    htab_for_each(t, print_word_count);

    htab_free(t);
    return 0;
}
