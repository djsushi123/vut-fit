#include <stdio.h>
#include "htab.h"
#include "htab_structs.h"

/**
 * Finds the item with the given key in the table.
 * @param t the table to search in
 * @param key the key to search for
 * @return the htab_pair_t with the given key or NULL if not found
 */
htab_pair_t *htab_find(const htab_t *t, htab_key_t key) {
    size_t index = htab_hash_function(key) % t->arr_size;
//    printf("Searching... The key %s resolved to index %zu\n", key, index);
    htab_item_t *item = t->arr_ptr[index];
    while (item != NULL) {
//        printf("searching... So far, I found key: %s\n", item->pair.key);
        if (strcmp(item->pair.key, key) == 0) {
            return &item->pair;
        }
        item = item->next;
    }
    return NULL;
}
