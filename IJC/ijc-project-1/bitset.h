// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#ifndef BITSET_H
#define BITSET_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include "error.h"

typedef unsigned long *bitset_t;
typedef unsigned long bitset_index_t;

#define bitset_create(array_name, array_size) \
    static_assert(array_size > 0, "bitset_create: Velikost musi byt >0."); \
    bitset_index_t array_name[((array_size - 1) / (CHAR_BIT * sizeof(bitset_index_t))) + 2] = { 0 }; \
    array_name[0] = array_size

#define bitset_alloc(array_name, array_size) \
    assert(array_size > 0);                  \
    bitset_t array_name = calloc(array_size + 1, sizeof(bitset_index_t)); \
    if (array_name == NULL) {                \
        error_exit("bitset_alloc: Chyba alokace pameti\n"); \
    }                                        \
    array_name[0] = array_size

#ifdef USE_INLINE

inline bitset_index_t bitset_size(bitset_t array_name) {
    return array_name[0];
}

inline void bitset_setbit(bitset_t array_name, bitset_index_t index, int expression) {
    if (index >= bitset_size(array_name)) {
        error_exit("Index larger than size.\n");
    }
    if (expression) {
        array_name[index / (sizeof(bitset_index_t) * CHAR_BIT) + 1] |= 1UL << (index % (sizeof(bitset_index_t) * CHAR_BIT));
    } else {
        array_name[index / (sizeof(bitset_index_t) * CHAR_BIT) + 1] &= ~(1UL << index % (sizeof(bitset_index_t) * CHAR_BIT));
    }
}

inline unsigned long bitset_getbit(bitset_t array_name, bitset_index_t index) {
    return ((index >= bitset_size(array_name)) ?
        (error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu\n", index, bitset_size(array_name) - 1), 0UL)
    : (array_name[index / (sizeof(bitset_index_t) * CHAR_BIT) + 1] >> (index % (sizeof(bitset_index_t) * CHAR_BIT))) & 1UL);
}

#else

#define FORCE_SEMICOLON static_assert(1, "")

#define bitset_free(array_name) \
    free(array_name)

#define bitset_size(array_name) \
    (array_name[0])

#define bitset_setbit(array_name, index, expression) \
    if (index >= bitset_size(array_name)) {          \
        error_exit("Index larger than size.\n");     \
    }                                                \
    if (expression) {                                \
        array_name[index / (sizeof(bitset_index_t) * CHAR_BIT) + 1] |= 1UL << (index % (sizeof(bitset_index_t) * CHAR_BIT)); \
    } else {                                         \
        array_name[index / (sizeof(bitset_index_t) * CHAR_BIT) + 1] &= ~(1UL << index % (sizeof(bitset_index_t) * CHAR_BIT)); \
    }                                                \
    FORCE_SEMICOLON

#define bitset_getbit(array_name, index) \
    ((index >= bitset_size(array_name)) ? \
        (error_exit("bitset_getbit: Index %lu mimo rozsah 0..%lu\n", index, bitset_size(array_name) - 1), 0UL) \
    : (array_name[index / (sizeof(bitset_index_t) * CHAR_BIT) + 1] >> (index % (sizeof(bitset_index_t) * CHAR_BIT))) & 1UL)

#endif // USE_INLINE
#endif // IJC_PROJECT_1_BITSET_H