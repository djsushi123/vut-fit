// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm.h"
#include "error.h"

struct ppm *ppm_read(const char *filename) {
    FILE *file = fopen(filename, "rb");

    if (file == NULL) {
        error_exit("ppm_read: chyba pri otvarani suboru.\n");
    }

    char fileFormat[3];
    unsigned int xsize, ysize, maxColor;

    if (fscanf(file, "%2s %u %u %u \n", fileFormat, &xsize, &ysize, &maxColor) < 4) {
        fclose(file);
        warning("ppm_read: hlavicka suboru nema spravny format.\n");
        return NULL;
    }

    if (strcmp(fileFormat, "P6")) {
        fclose(file);
        warning("ppm_read: subor nema format P6.\n");
        return NULL;
    }

    if (maxColor != 255 || xsize > 16000 || ysize > 16000) {
        fclose(file);
        warning("ppm_read: chybna hlavicka; max farba nie je 255 alebo presiahnuta xsize alebo ysize.\n");
        return NULL;
    }

    // all the space fot the entire thing so we can free it all at once
    struct ppm *image = malloc(3 * xsize * ysize * sizeof(char) + sizeof(struct ppm));
    if (image == NULL) {
        fclose(file);
        error_exit("ppm_read: chyba pri alokacii pamati pre struct ppm.\n");
    }

    if (fread(image->data, sizeof(char), 3 * xsize * ysize, file) != 3 * xsize * ysize) {
        fclose(file);
        ppm_free(image);
        error_exit("ppm_read: nespravne nacitalo data zo suboru.\n");
    }

    if (fgetc(file) != EOF) {
        fclose(file);
        ppm_free(image);
        error_exit("ppm_read: posledny char musi byt EOF.\n");
    }

    fclose(file);
    image->xsize = xsize;
    image->ysize = ysize;
    return image;
}

void ppm_free(struct ppm *p) {
    free(p);
}