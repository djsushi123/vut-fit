// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#ifndef IJC_PROJECT_1_PPM_H
#define IJC_PROJECT_1_PPM_H

struct ppm {
    unsigned xsize;
    unsigned ysize;
    char data[];
};

struct ppm *ppm_read(const char *filename);

void ppm_free(struct ppm *p);

#endif // IJC_PROJECT_1_PPM_H
