// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#include <math.h>
#include "bitset.h"
#include "eratosthenes.h"

void Eratosthenes(bitset_t bitset) {
    bitset_setbit(bitset, 0, 1);
    bitset_setbit(bitset, 1, 1);

    for (bitset_index_t j = 2; j <= (bitset_index_t) sqrt(bitset_size(bitset)); j++) {
        if (!bitset_getbit(bitset, j)) {
            for (bitset_index_t i = 2 * j; i < bitset_size(bitset); i += j) {
                bitset_setbit(bitset, i, 1);
            }
        }
    }
}