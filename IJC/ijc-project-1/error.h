// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#ifndef IJC_PROJECT_1_ERROR_H
#define IJC_PROJECT_1_ERROR_H

void warning(const char *fmt, ...);

void error_exit(const char *fmt, ...);

#endif //IJC_PROJECT_1_ERROR_H
