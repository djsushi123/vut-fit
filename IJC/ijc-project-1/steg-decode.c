// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#include <stdio.h>
#include <stdbool.h>
#include "ppm.h"
#include "bitset.h"
#include "eratosthenes.h"


#define PRIME_START 101

int main(int argc, char **argv) {

    if (argc != 2) {
        error_exit("treba passnut subor ako parameter do programu.\n");
    }

    struct ppm *image = ppm_read(argv[1]);

    if (image == NULL) {
        error_exit("zly format suboru ppm.\n");
    }

    bitset_alloc(bitset, 3 * image->xsize * image->ysize);
    Eratosthenes(bitset);

    bool ending = false;
    unsigned int currentBit = 0;
    char currentByte = 0;
    for (unsigned int i = PRIME_START; i < bitset_size(bitset); ++i) {
        if (bitset_getbit(bitset, i) == 1) continue;
        currentByte |= (image->data[i] & 1) << currentBit;
        ++currentBit;

        if (currentBit != sizeof(char) * CHAR_BIT) continue;
        if (currentByte == 0) {
            printf("\n");
            ending = true;
            break;
        }
        printf("%c", currentByte);
        currentByte = 0;
        currentBit = 0;
    }

    ppm_free(image);
    bitset_free(bitset);

    if (!ending) error_exit("secret message nema \\0 na konci.\n");
}