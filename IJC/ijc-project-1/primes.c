// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#include <limits.h>
#include <stdio.h>
#include <time.h>
#include "bitset.h"
#include "eratosthenes.h"

#define SIZE 230000000

int main() {
    clock_t start = clock();

    bitset_create(field, SIZE);
    Eratosthenes(field);

    bitset_index_t to_print[10];
    int innerIndex = 0;
    for (unsigned long i = SIZE - 1; i > 0; --i) {
        if (bitset_getbit(field, i) == 0) {
            to_print[innerIndex] = i;
            ++innerIndex;
            if (innerIndex == 10) break;
        }
    }

    for (int i = 9; i >= 0; --i) {
        printf("%lu\n", to_print[i]);
    }

    fprintf(stderr, "Time=%.3g\n", (double) (clock() - start) / CLOCKS_PER_SEC);

    return 0;
}
