// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "error.h"

void warning(const char *fmt, ...) {
    va_list arguments;
    va_start(arguments, fmt);
    fprintf(stderr, "Warning: ");
    vfprintf(stderr, fmt, arguments);
    va_end(arguments);
}

void error_exit(const char *fmt, ...) {
    va_list arguments;
    va_start(arguments, fmt);
    fprintf(stderr, "Error: ");
    vfprintf(stderr, fmt, arguments);
    va_end(arguments);
    exit(1);
}