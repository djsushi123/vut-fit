// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#include "bitset.h"

#ifdef USE_INLINE
extern unsigned long bitset_size(const bitset_t name);
extern void bitset_setbit(bitset_t name, const bitset_index_t index, const int value);
extern unsigned long bitset_getbit(const bitset_t name, const bitset_index_t index);
#endif