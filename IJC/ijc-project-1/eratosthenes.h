// Řešení IJC-DU1, 22.03.2023
// Autor: Martin Gaens, FIT
// Přeloženo: gcc 12.2

#ifndef IJC_PROJECT_1_ERATOSTHENES_H
#define IJC_PROJECT_1_ERATOSTHENES_H

#include "bitset.h"

void Eratosthenes(bitset_t bitset);

#endif //IJC_PROJECT_1_ERATOSTHENES_H